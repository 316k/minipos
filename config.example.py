# TODO : Copy this file as `config.py`

# Fill the blanks here to configure your installation :

COMPANY_NAME = 'Example Company'
COMPANY_ADDRESS = '''
123 Fake Street
Montreal, QC, Canada
(555) 123-4567
'''

# Secrets : these should be kept secret and must not change once the
# website is live
SECRET_KEY = '' # TODO: Choose something long and random, eg.: *HSD*un2moienuduijk2...
SECURITY_PASSWORD_SALT = '' # TODO: Choose something long and random

# Ask for the access code after {ACCESS_CODE_DELAY} seconds of
# inactivity
#
# Default : 30*60s = 30 minutes
ACCESS_CODE_DELAY = 30 * 60

# Access codes for each level of trust. Each access code should be
# UNIQUE and is designed to be *short* (since it is prompted often)
#
# DO NOT REUSE A SENSITIVE PASSWORD HERE.  This is a small protection
# against having clients touching the POS while your back is turned in
# the shop, but assume people might see it when employees type it.
admin_access_code = 'root'   # Simple, but kept secret by admins
employee_access_code = 'abc' # Simple, known to all employees

# Taxes
TAXES = [
    # Note : keep the '' around taxes to avoid rounding problems
    # Tax number can be empty if you don't want to show it on receipts
    {'name': 'TPS', 'rate': '0.05', 'number': '123-TX-NUMBER'}, # Tax 1
    {'name': 'TVQ', 'rate': '0.09975', 'number': '456-TX-NUMBER'}, # Tax 2
]

# Cash  Demoninations
CASH_DENOMINATIONS = [
    # (Display, Numeric value)
    ('1¢', '0.01'),
    ('5¢', '0.05'),
    ('10¢', '0.10'),
    ('25¢', '0.25'),
    ('1$', '1.00'),
    ('2$', '2.00'),
    ('5$', '5.00'),
    ('10$', '10.00'),
    ('20$', '20.00'),
    ('50$', '50.00'),
    ('100$', '100.00'),
]

# Email setup
MAIL_USERNAME = 'test@email.com'
MAIL_PASSWORD = '*****'
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_DEFAULT_SENDER = MAIL_USERNAME # Usually the same as your username
MAIL_USE_TLS = False
MAIL_USE_SSL = True

# Most workorders don't have a special "type"
#
# Those that do are removed from the calendar and are instead shown in
# their own separate tab
EXTRA_WORKORDER_TYPES = [
    # ('special-order', {
    #     'icon': '<i class="fa fa-tag"></i>',
    #     'description_one': 'Special order',
    #     'description_group': 'Special orders',
    # }),
    ('for-sale', {
        'icon': '<i class="fa fa-bicycle"></i>',
        'description_one': 'Bike for sale',
        'description_group': 'Bikes for sale',
    }),
    ('volunteer-projects', {
        'icon': '<i class="fa fa-star"></i>',
        'description_one': 'Volunteer project',
        'description_group': 'Volunteer projects',
    }),
]

# You'll probably want to set this
import time
import os

os.environ['TZ'] = 'America/Montreal'
time.tzset()

# --------------------------------------------------
# These should be sensible defaults

DATABASE = {
    'name': os.path.dirname(os.path.abspath(__file__)) + '/database.db',
    'engine': 'peewee.SqliteDatabase',
    'pragmas': {
        'foreign_keys': 1,
        'journal_mode': 'WAL',
    }
}

DEBUG = False
USE_FLASK_TALISMAN = True

# Set to TRUE when debugging if you don't want to send emails by mistake
MAIL_SUPPRESS_SEND = False

# To get fuzzy strings search, ex.: Josca -> Joska, add the
# `spellfix.so` extension to the project root and set this to True
USE_SPELLFIX = False
