#!/usr/bin/env python3

from flask import Flask
from flask_talisman import Talisman
from flask_peewee.db import Database
from flask_mail import Mail

import config

from decimal import Decimal

app = Flask(__name__)
app.config.from_object(config)

app.config['SECURITY_TRACKABLE'] = True

# -- Transform some config values --
for tx in app.config['TAXES']:
    tx['rate'] = Decimal(tx['rate'])

from collections import OrderedDict
app.config['EXTRA_WORKORDER_TYPES'] = OrderedDict(app.config['EXTRA_WORKORDER_TYPES'])

# -- Flask Talisman --
SELF = "'self'"

# FIXME : Although this website is basically completely hidden behind
# a login page, it is a safer practice to disable unsafe-inline
# scripts and styles
UNSAFE_INLINE = "'unsafe-inline'"


if app.config['USE_FLASK_TALISMAN']:
    talisman = Talisman(app,
        force_https=not app.config['TESTING'],
        content_security_policy={
            'default-src': SELF,
            'img-src': [
                SELF,
                'data:',
            ],
            'script-src': [
                SELF,
                UNSAFE_INLINE,
            ],
            'style-src': [
                SELF,
                UNSAFE_INLINE,
            ],
        })


mail = Mail(app)
db = Database(app)

# For editdist3()
if app.config['USE_SPELLFIX']:
    db.database.load_extension('spellfix')

assert list(db.database.execute_sql('pragma foreign_keys'))[0][0] == 1

# Uncomment this to dump all SQL queries
# import logging
# logger = logging.getLogger('peewee')
# logger.setLevel(logging.DEBUG)
# logger.addHandler(logging.StreamHandler())
