// usage: node compile-handlebars.js > static/js/precompiled-templates.js

let Handlebars = require('./static/js/handlebars.min-v4.7.7.js');

let templates = {
'new_order_typeahead':
    '<div><a href="/workorder/new/{{id}}">' +
    '{{first_name}} {{last_name}}' +
    '<br>' +
    '<small class="text-muted">{{phone}}</small>' +
    '</a></div>',

'list_workorders_typeahead':
    '<div><a href="/workorder/{{id}}">' +
    '<span class="workorderstatus-badge badge" style="background-color: {{ status_color }};">{{ status_name }}</span>' +
    '<span class="badge {{type_color}}" style="margin-left: 4px"> {{ type_name }}</span>' +
    '<span style="margin-left: 12px">{{ client_name }}</span> ' +
    '<br>' +
    '<small class="text-muted">{{description}}</small>' +
    '</a></div>',

'list_clients_typeahead':
    '<div><a href="/client/{{id}}">' +
    '{{first_name}} {{last_name}}' +
    '<br>' +
    '<small class="text-muted">{{phone}}</small>' +
    '</a></div>',

'set_client_typeahead':
    '<div><a href="/workorder/set_client/{{workorder_id}}/{{id}}">' +
    '{{first_name}} {{last_name}}' +
    '<br>' +
    '<small class="text-muted">{{phone}}</small>' +
    '</a></div>',

'search_items_typeahead':
    '<div>' +
    '{{name}} ' +
    '<span class="badge bg-info debug">{{score}}</span>' +
    '<br>' +
    '<small class="text-muted"><b>{{format_currency price}}</b> {{category}} {{subcategory}}</small>' +
    '</div>',

'items_modal_row':
    "<tr>" +
    '<td class="add"><button class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>' +
    '<td>' +
    '{{name}} ' +
    '<span class="badge bg-secondary debug">{{score}}</span> ' +
    '</td>' +
    '<td>{{format_currency price}}</td>' +
    '</tr>',
};

console.log('let HANDLEBARS_PRECOMPILED_TEMPLATES = {}');

for(let key in templates) {
    console.log('HANDLEBARS_PRECOMPILED_TEMPLATES.' + key + ' = Handlebars.template(');
    console.log(Handlebars.precompile(templates[key]));
    console.log(');');
}
