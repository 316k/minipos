let HANDLEBARS_PRECOMPILED_TEMPLATES = {}
HANDLEBARS_PRECOMPILED_TEMPLATES.new_order_typeahead = Handlebars.template(
{"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div><a href=\"/workorder/new/"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":1,"column":29},"end":{"line":1,"column":35}}}) : helper)))
    + "\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"first_name") || (depth0 != null ? lookupProperty(depth0,"first_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"first_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":37},"end":{"line":1,"column":51}}}) : helper)))
    + " "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"last_name") || (depth0 != null ? lookupProperty(depth0,"last_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"last_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":52},"end":{"line":1,"column":65}}}) : helper)))
    + "<br><small class=\"text-muted\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"phone") || (depth0 != null ? lookupProperty(depth0,"phone") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"phone","hash":{},"data":data,"loc":{"start":{"line":1,"column":95},"end":{"line":1,"column":104}}}) : helper)))
    + "</small></a></div>";
},"useData":true}
);
HANDLEBARS_PRECOMPILED_TEMPLATES.list_workorders_typeahead = Handlebars.template(
{"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<a class=\"list-group-item list-group-item-action\" href=\"/workorder/"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":1,"column":67},"end":{"line":1,"column":73}}}) : helper)))
    + "\"><span class=\"workorderstatus-badge badge\" style=\"background-color: "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"status_color") || (depth0 != null ? lookupProperty(depth0,"status_color") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"status_color","hash":{},"data":data,"loc":{"start":{"line":1,"column":142},"end":{"line":1,"column":160}}}) : helper)))
    + ";\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"status_name") || (depth0 != null ? lookupProperty(depth0,"status_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"status_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":163},"end":{"line":1,"column":180}}}) : helper)))
    + "</span><span class=\"badge "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"type_color") || (depth0 != null ? lookupProperty(depth0,"type_color") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type_color","hash":{},"data":data,"loc":{"start":{"line":1,"column":206},"end":{"line":1,"column":220}}}) : helper)))
    + "\" style=\"margin-left: 4px\"> "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"type_name") || (depth0 != null ? lookupProperty(depth0,"type_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"type_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":248},"end":{"line":1,"column":263}}}) : helper)))
    + "</span><span style=\"margin-left: 12px\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"client_name") || (depth0 != null ? lookupProperty(depth0,"client_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"client_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":302},"end":{"line":1,"column":319}}}) : helper)))
    + "</span> <br><small class=\"text-muted\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"description") || (depth0 != null ? lookupProperty(depth0,"description") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"description","hash":{},"data":data,"loc":{"start":{"line":1,"column":357},"end":{"line":1,"column":372}}}) : helper)))
    + "</small></a>";
},"useData":true}
);
HANDLEBARS_PRECOMPILED_TEMPLATES.list_clients_typeahead = Handlebars.template(
{"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div><a href=\"/client/"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":1,"column":22},"end":{"line":1,"column":28}}}) : helper)))
    + "\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"first_name") || (depth0 != null ? lookupProperty(depth0,"first_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"first_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":30},"end":{"line":1,"column":44}}}) : helper)))
    + " "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"last_name") || (depth0 != null ? lookupProperty(depth0,"last_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"last_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":45},"end":{"line":1,"column":58}}}) : helper)))
    + "<br><small class=\"text-muted\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"phone") || (depth0 != null ? lookupProperty(depth0,"phone") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"phone","hash":{},"data":data,"loc":{"start":{"line":1,"column":88},"end":{"line":1,"column":97}}}) : helper)))
    + "</small></a></div>";
},"useData":true}
);
HANDLEBARS_PRECOMPILED_TEMPLATES.set_client_typeahead = Handlebars.template(
{"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div><a href=\"/workorder/set_client/"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"workorder_id") || (depth0 != null ? lookupProperty(depth0,"workorder_id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"workorder_id","hash":{},"data":data,"loc":{"start":{"line":1,"column":36},"end":{"line":1,"column":52}}}) : helper)))
    + "/"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"id") || (depth0 != null ? lookupProperty(depth0,"id") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"id","hash":{},"data":data,"loc":{"start":{"line":1,"column":53},"end":{"line":1,"column":59}}}) : helper)))
    + "\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"first_name") || (depth0 != null ? lookupProperty(depth0,"first_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"first_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":61},"end":{"line":1,"column":75}}}) : helper)))
    + " "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"last_name") || (depth0 != null ? lookupProperty(depth0,"last_name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"last_name","hash":{},"data":data,"loc":{"start":{"line":1,"column":76},"end":{"line":1,"column":89}}}) : helper)))
    + "<br><small class=\"text-muted\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"phone") || (depth0 != null ? lookupProperty(depth0,"phone") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"phone","hash":{},"data":data,"loc":{"start":{"line":1,"column":119},"end":{"line":1,"column":128}}}) : helper)))
    + "</small></a></div>";
},"useData":true}
);
HANDLEBARS_PRECOMPILED_TEMPLATES.search_items_typeahead = Handlebars.template(
{"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<div>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"name","hash":{},"data":data,"loc":{"start":{"line":1,"column":5},"end":{"line":1,"column":13}}}) : helper)))
    + " <span class=\"badge bg-info debug\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"score") || (depth0 != null ? lookupProperty(depth0,"score") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"score","hash":{},"data":data,"loc":{"start":{"line":1,"column":48},"end":{"line":1,"column":57}}}) : helper)))
    + "</span><br><small class=\"text-muted\"><b>"
    + container.escapeExpression((lookupProperty(helpers,"format_currency")||(depth0 && lookupProperty(depth0,"format_currency"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"price") : depth0),{"name":"format_currency","hash":{},"data":data,"loc":{"start":{"line":1,"column":97},"end":{"line":1,"column":122}}}))
    + "</b> "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"category") || (depth0 != null ? lookupProperty(depth0,"category") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"category","hash":{},"data":data,"loc":{"start":{"line":1,"column":127},"end":{"line":1,"column":139}}}) : helper)))
    + " "
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"subcategory") || (depth0 != null ? lookupProperty(depth0,"subcategory") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"subcategory","hash":{},"data":data,"loc":{"start":{"line":1,"column":140},"end":{"line":1,"column":155}}}) : helper)))
    + "</small></div>";
},"useData":true}
);
HANDLEBARS_PRECOMPILED_TEMPLATES.items_modal_row = Handlebars.template(
{"compiler":[8,">= 4.3.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, lookupProperty = container.lookupProperty || function(parent, propertyName) {
        if (Object.prototype.hasOwnProperty.call(parent, propertyName)) {
          return parent[propertyName];
        }
        return undefined
    };

  return "<tr><td class=\"add\"><button class=\"btn btn-primary\"><i class=\"fa fa-plus-circle\"></i></button></td><td>"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"name") || (depth0 != null ? lookupProperty(depth0,"name") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"name","hash":{},"data":data,"loc":{"start":{"line":1,"column":103},"end":{"line":1,"column":111}}}) : helper)))
    + " <span class=\"badge bg-secondary debug\">"
    + container.escapeExpression(((helper = (helper = lookupProperty(helpers,"score") || (depth0 != null ? lookupProperty(depth0,"score") : depth0)) != null ? helper : container.hooks.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : (container.nullContext || {}),{"name":"score","hash":{},"data":data,"loc":{"start":{"line":1,"column":151},"end":{"line":1,"column":160}}}) : helper)))
    + "</span> </td><td>"
    + container.escapeExpression((lookupProperty(helpers,"format_currency")||(depth0 && lookupProperty(depth0,"format_currency"))||container.hooks.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? lookupProperty(depth0,"price") : depth0),{"name":"format_currency","hash":{},"data":data,"loc":{"start":{"line":1,"column":177},"end":{"line":1,"column":202}}}))
    + "</td></tr>";
},"useData":true}
);
