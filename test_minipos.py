import json
import pytest
import tempfile
import os
from decimal import Decimal
from peewee import fn
import datetime

# Hardcoded weird taxes, to make sure arbirary amounts are handled
# correctly
TAX1_RATE = Decimal('0.03')
TAX2_RATE = Decimal('0.07')
DATE_START = datetime.date.fromisoformat('1900-01-01')
DATE_END = datetime.date.fromisoformat('2100-01-01')

import models

ACCEPTED_PAYMENT_METHODS = models.CashRegisterState.TRACKED_PAYMENT_TYPES

def reset_modules():
    # XXX : This is a workaround for the fact that the current
    # application structure does not work well with unit tests. By not
    # using the create_app() pattern, and instead using global
    # variables in modules, launching each test with a fresh DB is
    # hard.  See: https://gitlab.com/316k/minipos/-/issues/58
    #
    #
    # The following code deletes local modules from python's cache, in
    # order to force them to be reimported
    #
    # Hack taken from : https://stackoverflow.com/a/66661311/14639652
    #
    # Note that the accepted solution on this SO thread does not work
    # with variables imported from modules that are not globally
    # imported, eg.: `from modules import Workorder` will not be
    # re-imported correctly)
    from glob import glob
    import sys

    # List local modules
    local_modules = [
        f[:-3] # strip ".py"
        for f in glob('*.py')
        if f != "test_minipos.py" # Don't reload the unit tests
    ]

    # Delete local modules from cache
    for mod in local_modules:
        if mod in sys.modules.keys():
            del sys.modules[mod]

@pytest.fixture()
def config():
    reset_modules()

    import config

    db_fd, db_path = tempfile.mkstemp()
    os.close(db_fd)
    os.unlink(db_path)

    config.DATABASE['name'] = db_path
    config.DATABASE['engine'] = 'peewee.SqliteDatabase'
    config.WTF_CSRF_ENABLED = False
    config.MAIL_SUPPRESS_SEND = True
    config.TESTING = True
    config.DEBUG = False

    config.SECRET_KEY = 'Test secret key'
    config.SECURITY_PASSWORD_SALT = 'Test password salt'

    config.admin_access_code = 'admin'
    config.employee_access_code = 'employee'

    config.TAXES = [
        {'name': 'TAX1', 'rate': TAX1_RATE, 'number': '1234'}, # Tax 1
        {'name': 'TAX2', 'rate': TAX2_RATE, 'number': '5678'}, # Tax 2
    ]

    yield config

    os.unlink(db_path)

@pytest.fixture()
def client(config):
    from app import app

    from security import user_datastore
    from flask_security import hash_password
    from create_db import create_db
    from models import Client

    with app.test_client() as client:

        with app.app_context():

            create_db()

            from models import Workorder

            if not user_datastore.find_user(email="pytest@pytest.com"):
                user_datastore.create_user(email="pytest@pytest.com", password=hash_password("pytest"))

        yield client

@pytest.fixture(scope="function")
def login(client):

    rv = client.post('/login', data=dict(
        email='pytest@pytest.com',
        password='pytest'
    ), follow_redirects=True)

    yield True

    rv = client.get('/logout', follow_redirects=True)

@pytest.fixture(scope="function")
def login_admin(client, login):
    client.post('/confirm-identity', data=dict(
        password='admin',
    ))

@pytest.fixture()
def empty_db():
    from models import Workorder, WorkorderItem, Transaction

    WorkorderItem.delete().where((WorkorderItem.refund_item.is_null(False))).execute()
    WorkorderItem.delete().execute()
    Transaction.delete().execute()
    Workorder.delete().execute()


def test_no_login(client):
    """Nothing should be reachable except /login"""

    for path in [
            '/',
            '/clients/',
            '/workorders/',
            '/whatever/',
            '/admin/',
            '/client/1']:

        rv = client.get(path, follow_redirects=False)
        assert rv.status_code != 200

        rv = client.get(path, follow_redirects=True)
        assert b'Login' in rv.data

def test_login(client):
    rv = client.post('/login', data=dict(
        email='pytest@pytest.com',
        password='pytest'
    ), follow_redirects=True)

    assert b'Logout' in rv.data

    rv = client.get('/logout', follow_redirects=True)
    assert b'Login' in rv.data

def test_no_500_errors(client, login):
    """There should be no 500 errors"""
    from models import Workorder, Client, InventoryItem

    sample_workorder = Workorder.select().where(~Workorder.paid).get()
    sample_paid_workorder = Workorder.select().where(Workorder.paid).get()
    sample_inventory_item = InventoryItem.select().order_by(fn.RANDOM()).get()
    sample_client = Client.get()

    for path in [
            '/',
            '/clients/',
            '/client/new',
            f'/client/new/{sample_workorder.id}',
            f'/client/new/{sample_paid_workorder.id}',
            '/workorders/',
            '/workorders/1',
            '/workorders/type/for-sale',
            '/workorders/type/for-sale/0',
            '/admin/',
            '/reports/',
            '/reports/?date_start=2099-01-01&date_end=2099-01-02',
            '/reports/?date_start=1999-01-01&date_end=2099-01-02',
            '/cash-register/',
            f'/client/{sample_client.id}',
            f'/workorder/{sample_paid_workorder.id}',
            f'/workorder/{sample_workorder.id}',
            f'/workorder/refund/{sample_paid_workorder.id}',
            f'/workorder/receipt/{sample_paid_workorder.id}',
            f'/api/total/{sample_paid_workorder.id}',
            f'/api/total/{sample_workorder.id}',
            '/api/search/clients/?query=twado',
            '/api/search/inventory_items/?query=pneu%20usa',
            '/favicon.ico',
            '/static/favicon.ico',
            '/admin/database-cleanup/',
            '/admin/client/',
            f'/admin/client/edit/?id={sample_client.id}',
            '/admin/inventoryitem/',
            f'/admin/inventoryitem/edit/?id={sample_inventory_item.id}',
            '/admin/workorder/',
            f'/admin/workorder/edit/?id={sample_workorder.id}',
            '/admin/workorderitem/',
            f'/admin/workorderitem/edit/?id={sample_paid_workorder.items.get().id}',
            '/admin/transaction/',
            f'/admin/transaction/edit/?id={sample_paid_workorder.items.get().id}',
    ]:

        print('Testing', path)
        rv = client.get(path, follow_redirects=True)

        assert rv.status_code == 200, f"Code {rv.status_code} on {path}"

def test_admin_only(client, login):
    """There should be no 500 errors"""
    from models import Workorder, Client, InventoryItem

    sample_workorder = Workorder.select().where(~Workorder.paid).get()
    sample_paid_workorder = Workorder.select().where(Workorder.paid).get()
    sample_inventory_item = InventoryItem.select().order_by(fn.RANDOM()).get()
    sample_client = Client.get()

    for path in [
            '/admin/',
            '/inventory/',
            '/reports/',
            '/reports/?date_start=2099-01-01&date_end=2099-01-02',
            '/reports/?date_start=1999-01-01&date_end=2099-01-02',
            '/cash-register/',
            '/admin/database-cleanup/',
            '/admin/client/',
            f'/admin/client/edit/?id={sample_client.id}',
            '/admin/inventoryitem/',
            f'/admin/inventoryitem/edit/?id={sample_inventory_item.id}',
            '/admin/workorder/',
            f'/admin/workorder/edit/?id={sample_workorder.id}',
            '/admin/workorderitem/',
            f'/admin/workorderitem/edit/?id={sample_paid_workorder.items.get().id}',
            '/admin/transaction/',
            f'/admin/transaction/edit/?id={sample_paid_workorder.items.get().id}',
    ]:

        rv = client.get(path, follow_redirects=True)
        assert rv.request.path == '/confirm-identity', f'{path} should redirect to /confirm-identity'


def most_recent_workorder():
    from models import Workorder

    return Workorder.select().order_by(Workorder.id.desc()).get()

def any_workorder(must_be_paid=None):
    from models import Workorder

    if must_be_paid is None:
        return Workorder.select().order_by(fn.RANDOM()).get()
    else:
        return (Workorder.select()
                        .where(Workorder.paid == must_be_paid)
                        .order_by(fn.RANDOM())
                        .get())

# def test_helpers():
#     print('---')
#     for i in range(3):
#         print(any_workorder().id)

#     for i in range(3):
#         assert any_workorder(must_be_paid=True).paid

#     for i in range(3):
#         assert not any_workorder(must_be_paid=False).paid

def test_mail(client, login):
    from models import Workorder

    # POST only
    workorder = any_workorder(must_be_paid=True)
    rv = client.get(f'/workorder/email-receipt/{workorder.id}')
    assert rv.status_code == 405

    # Emails for paid workorders only
    workorder = (Workorder.select()
                 .where(Workorder.paid == False)
                 .where(Workorder.client_id.is_null(False))
                 .get())
    assert workorder.client is not None
    rv = client.post(f'/workorder/email-receipt/{workorder.id}', data={
        'email': 'test@test.com'
    })
    assert rv.status_code == 403

    workorder = (Workorder.select()
                 .where(Workorder.paid == True)
                 .where(Workorder.client_id.is_null(False))
                 .get())
    assert workorder.client is not None
    rv = client.post(f'/workorder/email-receipt/{workorder.id}', data={
        'email': 'test@test.com'
    }, follow_redirects=True)
    assert rv.status_code == 200

    # Should work on both direct sales and workorders
    workorder = (Workorder.select()
                 .where(Workorder.paid == True)
                 .where(Workorder.client_id.is_null(True))
                 .get())
    assert workorder.client is None
    rv = client.post(f'/workorder/email-receipt/{workorder.id}', data={
        'email': 'test@test.com'
    }, follow_redirects=True)
    assert rv.status_code == 200

    # Shouldn't systematically save email
    workorder = (Workorder.select()
                 .where(Workorder.paid == True)
                 .where(Workorder.client_id.is_null(False))
                 .get())
    email_before = workorder.client.email
    rv = client.post(f'/workorder/email-receipt/{workorder.id}', data={
        'email': 'test@test.com'
    }, follow_redirects=True)
    assert rv.status_code == 200

    Workorder.get(workorder.id).client.email == email_before

    # Should save email if queried
    workorder = (Workorder.select()
                 .where(Workorder.paid == True)
                 .where(Workorder.client_id.is_null(False))
                 .get())
    assert workorder.client.email != 'twaaoisajdoiasjdoijsad@test.com'
    rv = client.post(f'/workorder/email-receipt/{workorder.id}', data={
        'email': 'twaaoisajdoiasjdoijsad@test.com'
    }, follow_redirects=True)
    assert rv.status_code == 200

    Workorder.get(workorder.id).client.email == 'twaaoisajdoiasjdoijsad@test.com'

def test_delete_workorder(client, login):
    from models import Workorder, InventoryItem, WorkorderItem

    # Simple create/delete of an empty workorder
    orig_count = Workorder.select().count()

    rv = client.get('/workorder/new/direct', follow_redirects=True)
    assert rv.status_code == 200

    assert Workorder.select().count() == orig_count + 1

    id = most_recent_workorder().id

    rv = client.get('/workorder/delete/' + str(id), follow_redirects=True)

    assert rv.status_code == 200
    assert Workorder.select().count() == orig_count


def test_refund_simple(client, login):
    from models import Workorder, WorkorderItem

    # FIXME : this makes assumptions about the database, it might not
    # always be correct
    workorder = Workorder.select().order_by(Workorder.id.asc()).get()
    workorder_count = Workorder.select().count()

    refunded_items = []
    for item in workorder.items:
        refunded_items.append(item.id)

    data = {
        'refunded_items_id':refunded_items,
        'workorder_id':workorder.id
    }

    json_data = json.dumps(data)
    rv = client.post('/api/refund/', data=dict(items=json_data), follow_redirects=True)
    refunded_workorder = most_recent_workorder()

    assert Workorder.select().count() == workorder_count + 1

    assert workorder.client_id == refunded_workorder.client_id
    assert workorder.bike_description == refunded_workorder.bike_description
    assert workorder.bike_serial_number == refunded_workorder.bike_serial_number

    for i in range(len(workorder.items)):
        assert workorder.items[i].nb == -refunded_workorder.items[i].nb


def test_refund_cornercases(client, login):
    from models import Workorder, WorkorderItem, InventoryItem

    item1 = InventoryItem.select(InventoryItem.id).where(~InventoryItem.archived).offset(0).get()
    item2 = InventoryItem.select(InventoryItem.id).where(~InventoryItem.archived).offset(1).get()

    # Creates workorder with 2 items
    rv = client.get('/workorder/new/direct', follow_redirects=True)
    workorder = most_recent_workorder()
    rv = client.get(f'/api/add-item/{workorder.id}/{item1.id}', follow_redirects=True)
    rv = client.get(f'/api/add-item/{workorder.id}/{item2.id}', follow_redirects=True)

    # Redirected if workorder is not paid
    rv = client.get(f'/workorder/refund/{workorder.id}', follow_redirects=False)
    assert not workorder.paid and rv.status_code == 302

    rv = client.post(f'/workorder/pay/{workorder.id}', data=dict(payment_method='cash'), follow_redirects=True)
    workorder = Workorder.get(workorder.id)

    rv = client.get(f'/workorder/refund/{workorder.id}', follow_redirects=False)
    assert workorder.paid and rv.status_code == 200


    # Cannot refund an item twice
    data = {
        'refunded_items_id': [workorder.items[0].id],
        'workorder_id':workorder.id
    }
    json_data = json.dumps(data)

    rv = client.post('/api/refund/', data=dict(items=json_data), follow_redirects=True)
    workorder_count =  Workorder.select().count()
    workorderitem_count = WorkorderItem.select().count()

    rv = client.post('/api/refund/', data=dict(items=json_data), follow_redirects=True)
    assert workorder_count == Workorder.select().count()
    assert workorderitem_count == WorkorderItem.select().count()


    # Cannnot refund a refund
    refund_workorder = most_recent_workorder()
    rv = client.post(f'/workorder/pay/{refund_workorder.id}', data={'payment_method': 'cash'}, follow_redirects=True)

    data = {
        'refunded_items_id':[refund_workorder.items[0].id],
        'workorder_id':refund_workorder.id
    }
    json_data = json.dumps(data)
    workorder_count = Workorder.select().count()
    workorderitem_count = WorkorderItem.select().count()

    rv = client.post('/api/refund/', data=dict(items=json_data), follow_redirects=True)
    assert workorder_count == Workorder.select().count()
    assert workorderitem_count == WorkorderItem.select().count()


    # Refund button is only available is workorder is not fully refunded
    rv = client.get(f'/workorder/{workorder.id}', follow_redirects=True)
    assert b'id="refund-button"' in rv.data

    data = {
        'refunded_items_id':[workorder.items[1].id],
        'workorder_id':workorder.id
    }
    json_data = json.dumps(data)
    rv = client.post('/api/refund/', data=dict(items=json_data), follow_redirects=True)

    rv = client.get(f'/workorder/{workorder.id}', follow_redirects=True)
    assert b'id="refund-button"' not in rv.data

    # 4b. Redirected if workorder is fully refunded
    rv = client.get(f'/workorder/refund/{workorder.id}', follow_redirects=False)
    for item in workorder.items:
        serialized_item = item.serialized()
        assert serialized_item['refunded_in_workorder_id'] and rv.status_code == 302

def test_payment(client, login):
    from models import Workorder, Transaction, InventoryItem
    import config

    payment_methods = ['cash', 'interac', 'visa']

    for payment_method in payment_methods:
        rv = client.get('/workorder/new/direct', follow_redirects=True)
        workorder_id = most_recent_workorder().id

        inventory_item = InventoryItem.select().order_by(InventoryItem.id.asc()).where(InventoryItem.taxable).get()

        rv = client.get(f'/api/add-item/{workorder_id}/{inventory_item.id}', follow_redirects=True)

        rv = client.post(f'/workorder/pay/{workorder_id}', data=dict(payment_method=payment_method), follow_redirects=True)

        workorder = Workorder.get(workorder_id)

        assert workorder.paid_subtotal == inventory_item.price
        assert workorder.paid_tax1_rate == TAX1_RATE
        assert workorder.paid_tax2_rate == TAX2_RATE
        assert workorder.paid_taxes1 == round(workorder.paid_subtotal * TAX1_RATE, 2)
        assert workorder.paid_taxes2 == round(workorder.paid_subtotal * TAX2_RATE, 2)
        assert workorder.paid_total == workorder.paid_subtotal + workorder.paid_taxes1 + workorder.paid_taxes2
        assert workorder.paid

        assert Transaction.select().where(Transaction.workorder_id == workorder_id).count() == 1

    # TODO : Payment for workorders with weird taxes (eg.: two taxable
    # items, one non-taxable)

def test_payment_zero_dollar(client, login):
    from models import Workorder, Transaction, InventoryItem

    rv = client.get('/workorder/new/direct', follow_redirects=True)
    workorder = most_recent_workorder()

    assert workorder.transactions.count() == 0
    assert workorder.items.count() == 0

    # Empty workorders should not be payable
    rv = client.post(f'/workorder/pay/{workorder.id}', data=dict(payment_method='visa'), follow_redirects=True)
    assert rv.status_code != 200

    # 0$ total workorders should be payable
    InventoryItem.insert(name='Testing', keywords='',
                         price=0.00, avg_cost=0.0, current_cost=0.0, msrp=0,
                         taxable=True, avg_time_spent=0).execute()


    inventory_item = InventoryItem.select().order_by(InventoryItem.id.desc()).get()

    rv = client.get(f'/api/add-item/{workorder.id}/{inventory_item.id}', follow_redirects=True)
    assert rv.status_code == 200

    rv = client.post(f'/workorder/pay/{workorder.id}', data=dict(payment_method='cash'), follow_redirects=True)
    assert rv.status_code == 200

    workorder = Workorder.get(workorder.id) # Reload
    assert workorder.paid

    # 0$ workorders should not create a transaction
    assert workorder.transactions.count() == 0



    # Don't repay the same workorder twice
    rv = client.post(f'/workorder/pay/{workorder.id}', data=dict(payment_method='visa'), follow_redirects=True)
    assert rv.status_code != 200

    # Items from a paid workorder should not be editable via the api
    rv = client.get(f'/api/add-item/{workorder.id}/{inventory_item.id}', follow_redirects=True)
    assert rv.status_code != 200

def test_compare_workorder_stats(client, login):
    from models import Workorder
    from views import compute_workorder_stats, compute_workorder_stats_slow

    # Test extreme dates : the SQL query should yield the same results
    # as calling the functions one by one
    for date_start, date_end in [
            ('1900-01-01', '1900-01-01'),
            ('1900-01-01', '2100-01-01'),
            ('2100-01-01', '2100-01-01'),
    ]:
        date_start = datetime.date.fromisoformat(date_start)
        date_end = datetime.date.fromisoformat(date_end)

        slow = compute_workorder_stats_slow(date_start, date_end)
        fast = compute_workorder_stats(date_start, date_end)

        assert slow == fast

    # Test random dates
    for i in range(10):
        dates = Workorder.select().where(Workorder.paid_date.is_null(False)).order_by(fn.RANDOM()).limit(2)
        min_date = str(min([d.paid_date for d in dates]))
        max_date = str(max([d.paid_date for d in dates]))

        for date_start, date_end in [
                ('1900-01-01', min_date),
                ('1900-01-01', max_date),
                (min_date, '2100-01-01'),
                (max_date, '2100-01-01'),
                (min_date, max_date),
        ]:
            date_start = datetime.datetime.fromisoformat(date_start)
            date_end = datetime.datetime.fromisoformat(date_end)

            slow = compute_workorder_stats_slow(date_start, date_end)
            fast = compute_workorder_stats(date_start, date_end)

            print(date_start, date_end)
            print(slow)
            print(fast)
            assert slow == fast

def create_simple_membership_workorder(client):
    from models import Workorder, Transaction, InventoryItem

    inventory_item = InventoryItem.select().where(InventoryItem.name == 'Abonnement Simple').get()
    assert inventory_item.price == Decimal('4.99')
    assert inventory_item.taxable

    rv = client.get('/workorder/new/direct', follow_redirects=True)
    workorder = most_recent_workorder()
    rv = client.get(f'/api/add-item/{workorder.id}/{inventory_item.id}', follow_redirects=True)

    assert workorder.subtotal() == Decimal('4.99')
    assert workorder.total() == Decimal('5.49')

    return workorder

def test_multiple_payments_balance(client, login):
    from models import Workorder, Transaction, InventoryItem

    def pay_multiple(workorder, payments, expected_transactions_nb):
        transaction_count = Transaction.select().count()

        rv = client.post(f'/workorder/pay/{workorder.id}', data=payments, follow_redirects=True)
        assert rv.status_code == 200
        workorder = Workorder.get(workorder.id)

        assert workorder.paid_subtotal == Decimal('4.99')
        assert workorder.paid_tax1_rate == TAX1_RATE
        assert workorder.paid_tax2_rate == TAX2_RATE
        assert workorder.paid_taxes1 == round(workorder.paid_subtotal * TAX1_RATE, 2)
        assert workorder.paid_taxes2 == round(workorder.paid_subtotal * TAX2_RATE, 2)
        assert workorder.paid_total == Decimal('5.49')
        assert workorder.paid

        assert Transaction.select().count() == transaction_count + expected_transactions_nb

    # Simple membership : 4.99 + tx = 5.49
    pay_multiple(create_simple_membership_workorder(client), {'interac': '5.49'}, 1)
    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.49'}, 1)
    pay_multiple(create_simple_membership_workorder(client), {'cash': '5.49'}, 1)

    pay_multiple(create_simple_membership_workorder(client), {'interac': '0.49', 'visa': '5.00'}, 2)
    pay_multiple(create_simple_membership_workorder(client), {'visa': '0.09', 'cash': '5.40'}, 2)
    pay_multiple(create_simple_membership_workorder(client), {'cash': '5.09', 'interac': '0.40'}, 2)

    pay_multiple(create_simple_membership_workorder(client), {'cash': '1.49', 'visa': '2', 'interac': '2'}, 3)
    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.00', 'interac': '0.48', 'cash': '0.01'}, 3)

    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.00', 'interac': '0.48', 'cash': '0.01', 'this-should-be-ignored': '10.55'}, 3)

    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.49', 'interac': '0.00'}, 1)
    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.49', 'interac': '0.00', 'cash': '0.00'}, 1)
    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.00', 'interac': '0.49', 'cash': '0.00'}, 2)


def test_multiple_payments_not_balance(client, login):
    from models import Workorder, Transaction

    def pay_multiple(workorder, payements):
        transaction_count = Transaction.select().count()

        rv = client.post(f'/workorder/pay/{workorder.id}', data=payements, follow_redirects=True)
        assert rv.status_code == 403
        assert transaction_count == Transaction.select().count()

        workorder = Workorder.get(workorder.id)
        assert not workorder.paid

    # Simple membership : 4.99 + tx = 5.49

    # -- Not enough --
    pay_multiple(create_simple_membership_workorder(client), {'cash': '0'})
    pay_multiple(create_simple_membership_workorder(client), {'visa': '0'})
    pay_multiple(create_simple_membership_workorder(client), {'interac': '0'})

    pay_multiple(create_simple_membership_workorder(client), {'cash': '1.00'})
    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.00'})
    pay_multiple(create_simple_membership_workorder(client), {'interac': '2.00'})

    pay_multiple(create_simple_membership_workorder(client), {'visa': '-5.49'})

    pay_multiple(create_simple_membership_workorder(client), {'cash': '1.00', 'visa': '3.99'})
    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.00', 'interac': '0.48'})

    pay_multiple(create_simple_membership_workorder(client), {'visa': '4.99', 'interac': '0.48', 'cash': '0.01'})
    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.00', 'this-should-be-ignored': '0.49'})

    # -- Too much --
    pay_multiple(create_simple_membership_workorder(client), {'cash': '100'})
    pay_multiple(create_simple_membership_workorder(client), {'visa': '100'})
    pay_multiple(create_simple_membership_workorder(client), {'interac': '100'})

    pay_multiple(create_simple_membership_workorder(client), {'cash': '125.49'})
    pay_multiple(create_simple_membership_workorder(client), {'visa': '55.49'})
    pay_multiple(create_simple_membership_workorder(client), {'interac': '5.50'})

    pay_multiple(create_simple_membership_workorder(client), {'cash': '5.49', 'visa': '5.49'})

    pay_multiple(create_simple_membership_workorder(client), {'visa': '5.00', 'interac': '0.48', 'cash': '0.02'})
    pay_multiple(create_simple_membership_workorder(client), {'visa': '6.49', 'this-should-be-ignored': '-1.00'})

def test_refund_multiple_payments(client, login):
    from models import Workorder, Transaction

    # Simple membership : 4.99 + tx = 5.49
    # Creates workorder and pays it
    workorder = create_simple_membership_workorder(client)
    rv = client.post(f'/workorder/pay/{workorder.id}', data=dict(payment_method='cash'), follow_redirects=True)
    workorder = Workorder.get(workorder.id)
    assert workorder.paid

    # Creates refund workorder
    data = {
        'refunded_items_id': [workorder.items[0].id],
        'workorder_id':workorder.id
    }
    json_data = json.dumps(data)
    rv = client.post('/api/refund/', data=dict(items=json_data), follow_redirects=True)
    refund_workorder = most_recent_workorder()
    assert workorder.items[0].nb == -refund_workorder.items[0].nb

    # Gives perfect amount
    transaction_count = Transaction.select().count()
    payments =  {'visa': '4.99', 'interac': '0.49', 'cash': '0.01'}
    rv = client.post(f'/workorder/pay/{refund_workorder.id}', data=payments, follow_redirects=True)
    assert rv.status_code == 200
    assert Transaction.select().count() == transaction_count + len(payments.keys())

    refund_workorder = Workorder.get(refund_workorder.id)
    assert refund_workorder.paid

    # Checks if amounts are perfectly inversed
    refund_transaction = Transaction.select(Transaction.amount).where(Transaction.workorder_id == refund_workorder.id)

    for method in payments.keys():
        amount = refund_transaction.where(Transaction.payment_method == method).scalar()
        assert Decimal(payments[method]) == -amount


def test_purchase_with_in_store_credit(client, login):
    from models import Workorder, InventoryItem, Transaction, Client

    client_id = Client.select().order_by(Client.id.desc()).scalar()
    rv = client.get(f'/workorder/new/{client_id}', follow_redirects=True)
    workorder = most_recent_workorder()

    inventory_item = InventoryItem.select().order_by(InventoryItem.id.asc()).where(InventoryItem.taxable).get()
    rv = client.get(f'/api/add-item/{workorder.id}/{inventory_item.id}', follow_redirects=True)

    workorder = Workorder.get(workorder.id)
    rv = client.post(f'/make-deposit/{workorder.id}', data={'amount' : workorder.total(), 'deposit_type': 'cash'}, follow_redirects=True)
    rv = client.post(f'/workorder/pay/{workorder.id}', data={'credit account': workorder.total()}, follow_redirects=True)

    workorder = Workorder.get(workorder.id)

    assert workorder.paid_subtotal == inventory_item.price
    assert workorder.paid
    workorder.test_invariants()

    assert Transaction.select().where(Transaction.workorder_id == workorder.id).count() == 1

def create_workorder_with_two_items(client):
    from models import Workorder, InventoryItem, Client

    client_id = Client.select().order_by(Client.id.desc()).scalar()
    rv = client.get(f'/workorder/new/{client_id}', follow_redirects=True)
    workorder_id = most_recent_workorder().id

    item1 = InventoryItem.select().where(InventoryItem.name == 'Abonnement Simple').get()
    item2 = InventoryItem.select().where(InventoryItem.name == 'Abonnement Premium').get()

    assert item1.price + item2.price == Decimal('29.98')
    rv = client.get(f'/api/add-item/{workorder_id}/{item1.id}', follow_redirects=True)
    rv = client.get(f'/api/add-item/{workorder_id}/{item2.id}', follow_redirects=True)

    workorder = Workorder.get(workorder_id)
    return workorder


def refund_items(client, payment_method, workorder_id, item_ids):

    data = {
        'refunded_items_id': item_ids,
        'workorder_id': workorder_id
    }

    json_data = json.dumps(data)
    rv = client.post('/api/refund/', data=dict(items=json_data), follow_redirects=True)
    refunded_workorder = most_recent_workorder()
    rv = client.post(f'/workorder/pay/{refunded_workorder.id}', data=payment_method, follow_redirects=True)

    return most_recent_workorder()

def test_reports_cash_register_balance_simple_transaction(client, login, empty_db):
    """Test the effect of simple payments on cash register"""

    from models import Workorder, Transaction
    from views import compute_workorder_stats, compute_cash_register_stats

    # Simple transaction (paid with a single payment method)

    for method in ACCEPTED_PAYMENT_METHODS:
        workorder_id = create_workorder_with_two_items(client).id
        rv = client.post(f'/workorder/pay/{workorder_id}', data=dict(payment_method=method), follow_redirects=True)

        # Two items : 29.98 + tx = 32.98
        workorder = Workorder.get(workorder_id)
        assert workorder.paid_subtotal == Decimal('29.98')
        assert workorder.paid_total == Decimal('32.98')
        assert workorder.paid_total == workorder.paid_subtotal + workorder.paid_taxes1 + workorder.paid_taxes2
        assert workorder.paid

        assert Transaction.select().where(Transaction.workorder_id == workorder_id).count() == 1

    cash_register_stats = compute_cash_register_stats()
    workorder_stats = compute_workorder_stats(DATE_START, DATE_END)

    # Only payments, no deposits, the cash register == the total gains
    assert workorder_stats['total'] == cash_register_stats['total'] == Decimal('32.98') * len(ACCEPTED_PAYMENT_METHODS)


def test_reports_cash_register_balance_multiple_transactions(client, login):
    """Test the effect of doing multiple-payments-methods at once on cash
    register

    """

    from models import Workorder, Transaction
    from views import compute_workorder_stats, compute_cash_register_stats

    old_stats = compute_workorder_stats(DATE_START, DATE_END)
    old_cash_register = compute_cash_register_stats()

    # Multiple transactions
    old_transaction_count = Transaction.select().count()
    expected_transactions_nb = len(ACCEPTED_PAYMENT_METHODS)

    # Pay with multiple payment methods
    # Two items : 29.98 + tx = 32.98
    workorder_id = create_workorder_with_two_items(client).id
    rv = client.post(f'/workorder/pay/{workorder_id}', data= {'visa': '32.00', 'interac': '0.50', 'cash': '0.48'}, follow_redirects=True)

    workorder = Workorder.get(workorder_id)
    assert workorder.paid
    assert workorder.paid_subtotal == Decimal('29.98')
    assert workorder.paid_total == Decimal('32.98')
    workorder.test_invariants()

    # One transaction per payment method has been added
    assert Transaction.select().count() == old_transaction_count + expected_transactions_nb

    stats = compute_workorder_stats(DATE_START, DATE_END)
    cash_register = compute_cash_register_stats()

    assert cash_register['total'] == old_cash_register['total'] + Decimal('32.98')

def test_deposit_reports_cash_register_balance(client, login_admin):
    """Making a deposit in /cash-register/...

    - should NOT affect the workorder_stats (no extra costs/profits are made)
    - SHOULD affect the total collected money

    Depositing + withdrawing + the same amount should cancel out to 0$

    """
    from views import compute_workorder_stats, compute_cash_register_stats

    original_stats = compute_workorder_stats(DATE_START, DATE_END)
    original_cash_register = compute_cash_register_stats()

    def make_deposit_or_withdrawal(transaction_type, amount):
        before_cash_register = compute_cash_register_stats()
        before_cash = before_cash_register['cash']
        before_total_register = before_cash_register['total']

        rv = client.post(f'/cash-register/', data={'deposit': transaction_type, 'amount': amount, 'comment': ''}, follow_redirects=True)

        if transaction_type == 'withdrawal':
            amount *= -1

        # Checks that cash register changes, but reports do not
        stats = compute_workorder_stats(DATE_START, DATE_END)
        after_cash_register = compute_cash_register_stats()
        assert after_cash_register['cash'] == before_cash + amount
        assert after_cash_register['total'] == before_total_register + amount
        assert before_cash_register['interac'] == after_cash_register['interac']
        assert before_cash_register['visa'] == after_cash_register['visa']
        assert original_stats == stats

    # Make some deposits and withdrawals that sum to zero
    make_deposit_or_withdrawal('deposit', 20)
    make_deposit_or_withdrawal('deposit', 5)

    make_deposit_or_withdrawal('withdrawal', 10)
    make_deposit_or_withdrawal('withdrawal', 20)

    make_deposit_or_withdrawal('deposit', 5)

    stats = compute_workorder_stats(DATE_START, DATE_END)
    assert stats == original_stats

    # The amounts should sum to zero
    final_cash_register = compute_cash_register_stats()
    assert final_cash_register['total'] == original_cash_register['total']


def test_refund_reports_cash_register_balance(client, login):
    """Refunds...

    - SHOULD affect the cash register
    - SHOULD affect the profits/costs reports

    """
    from models import Workorder, Transaction
    from views import compute_workorder_stats, compute_cash_register_stats

    old_transaction_count = Transaction.select().count()
    old_stats = compute_workorder_stats(DATE_START, DATE_END)
    old_cash_register = compute_cash_register_stats()
    old_cash_register_total = old_cash_register['total']

    created_workorder_ids = []

    # Pay a few workorders with each single payment method
    for method in ACCEPTED_PAYMENT_METHODS:
        workorder = create_workorder_with_two_items(client)
        created_workorder_ids.append(workorder.id)
        rv = client.post(f'/workorder/pay/{workorder.id}', data=dict(payment_method=method), follow_redirects=True)
        assert Workorder.get(workorder.id).paid
        workorder.test_invariants()

    # This holds stats from the previous interaction
    previous_stats = compute_workorder_stats(DATE_START, DATE_END)
    previous_cash_register_total = compute_cash_register_stats()['total']

    # Refund each workorder with the same payment method it used
    for current_workorder_id, method in zip(created_workorder_ids, ACCEPTED_PAYMENT_METHODS):
        workorder = Workorder.get(current_workorder_id)

        refund_items(client, dict(payment_method=method), workorder.id, [workorder.items[0].id, workorder.items[1].id])

        current_stats = compute_workorder_stats(DATE_START, DATE_END)
        current_cash_register = compute_cash_register_stats()
        current_cash_register_total = current_cash_register['total']

        assert current_stats['total'] < previous_stats['total']
        assert current_stats['total'] == previous_stats['total'] - Decimal('32.98')

        assert current_cash_register_total < previous_cash_register_total
        assert current_cash_register_total ==  previous_cash_register_total - Decimal('32.98')

        previous_cash_register_total = current_cash_register_total
        previous_stats = current_stats.copy()

    # Everything has been refunded : total should be the same as
    # before adding the workorders
    assert current_stats == old_stats

    # The cash register should not have been afected overall
    assert current_cash_register_total == old_cash_register_total

    # There should be multiple new transactions in the database : one
    # payment + one refund = 2 transaction per tested payment_method
    assert Transaction.select().count() == old_transaction_count + 2 * len(ACCEPTED_PAYMENT_METHODS)


def test_in_store_credit_deposit(client, login):
    """A client's deposit (in-store-credit)...

    - SHOULD NOT affect the profits/costs stats
    - SHOULD affect the current cash register amounts
    """
    from models import Workorder
    from views import compute_workorder_stats, compute_cash_register_stats

    old_stats = compute_workorder_stats(DATE_START, DATE_END)
    old_cash_register = compute_cash_register_stats()
    old_cash = old_cash_register['cash']

    # Creates workorder and sets client
    workorder = create_workorder_with_two_items(client)

    # Deposits 20$ of cash as in-store-credit
    rv = client.post(f'/make-deposit/{workorder.id}', data={'amount' : '20', 'deposit_type': 'cash'}, follow_redirects=True)
    assert rv.status_code == 200

    current_stats = compute_workorder_stats(DATE_START, DATE_END)
    current_cash_register = compute_cash_register_stats()

    assert old_stats == current_stats
    # A deposit does not change the total money gained, when considering the 'credit accounts'
    # +20$ in cash
    # -20$ in credit account
    assert (
        old_cash_register['total'] + old_cash_register['credit account'] ==
        current_cash_register['total'] + current_cash_register['credit account'])
    assert current_cash_register['cash'] == old_cash + 20
    assert current_cash_register['credit account'] == old_cash_register['credit account'] - 20

def test_refund_with_in_store_credit(client, login, empty_db):
    """Refunding an item with in-store credit...

    - SHOULD affect the in-store credit of the client
    - SHOULD affect the costs/profits (less profits have been made after the refund)
    - SHOULD NOT affect the cash register amounts (all money is kept)

    It SHOULD NOT be possible to refund a direct sale as an in-store credit
    """
    from models import Workorder
    from views import compute_workorder_stats, compute_cash_register_stats

    workorder = create_workorder_with_two_items(client)

    original_in_store_credit = workorder.client.in_store_credit

    old_stats = compute_workorder_stats(DATE_START, DATE_END)
    old_cash_register = compute_cash_register_stats()
    old_cash_register_total = old_cash_register['total']

    # Pay cash
    rv = client.post(f'/workorder/pay/{workorder.id}', data=dict(payment_method="cash"), follow_redirects=True)
    assert Workorder.get(workorder.id).paid

    stats_after_pay = compute_workorder_stats(DATE_START, DATE_END)
    cash_register_after_pay = compute_cash_register_stats()
    cash_register_total_after_pay = cash_register_after_pay['total']

    assert stats_after_pay['total'] == old_stats['total'] + Decimal('32.98')
    assert cash_register_total_after_pay == old_cash_register_total + Decimal('32.98')

    # Refund the workorder as in-store credit
    refunded_workorder = refund_items(client, dict(refund_as_in_store_credit="1"), workorder.id, [workorder.items[0].id, workorder.items[1].id])
    stats_after_refund = compute_workorder_stats(DATE_START, DATE_END)
    cash_register_after_refund = compute_cash_register_stats()

    # In-store credit is correctly registered
    assert refunded_workorder.client.in_store_credit == original_in_store_credit + Decimal('32.98')

    # Less profits have been made after the refund
    assert stats_after_refund['total'] == stats_after_pay['total'] - Decimal('32.98')

    # All money is kept in the cash registered
    assert cash_register_total_after_pay == cash_register_after_refund['total']
    for stat in stats_after_refund:
        assert stats_after_refund[stat] == old_stats[stat]

def test_refund_with_all_payment_methods(client, login, empty_db):
    """Refunding with multiple payment methods at once should affect
    everything

    - Cash register changes
    - Profits change
    - In-store credit changes
    """
    from models import Client, Workorder
    from views import compute_workorder_stats, compute_cash_register_stats

    old_stats = compute_workorder_stats(DATE_START, DATE_END)
    old_cash_register = compute_cash_register_stats()

    workorder = create_workorder_with_two_items(client)

    old_credit = workorder.client.in_store_credit

    # ---------- Add 2$ as in-store credit for this client ----------
    rv = client.post(f'/make-deposit/{workorder.id}', data={'amount' : '2.00', 'deposit_type': 'cash'}, follow_redirects=True)
    cash_register_after_deposit = compute_cash_register_stats()

    assert cash_register_after_deposit['total'] == old_cash_register['total'] + Decimal('2.00')
    assert cash_register_after_deposit['cash'] == old_cash_register['cash'] + Decimal('2.00')
    assert cash_register_after_deposit['used_in_store_credit'] == old_cash_register['used_in_store_credit']
    assert cash_register_after_deposit['new_in_store_credit'] == old_cash_register['new_in_store_credit'] + Decimal('2.00')

    # ---------- Pay the workorder ----------
    methods_of_payment = {'visa': '30.00', 'interac': '0.50', 'cash': '0.48', 'credit account': '2.00'}
    rv = client.post(f'/workorder/pay/{workorder.id}', data=methods_of_payment, follow_redirects=True)
    workorder = Workorder.get(workorder.id)
    assert workorder.paid
    workorder.test_invariants()

    stats_after_pay = compute_workorder_stats(DATE_START, DATE_END)
    cash_register_after_pay = compute_cash_register_stats()
    assert stats_after_pay['total'] == old_stats['total'] + Decimal('32.98')

    # The 2$ credit has been spent
    c = Client.get(workorder.client)
    assert c.in_store_credit == old_credit
    assert cash_register_after_pay['used_in_store_credit'] == old_cash_register['used_in_store_credit'] + Decimal('2.00')

    # The actual money gained in the register does not include the 'credit account'
    assert cash_register_after_pay['total'] == (
        cash_register_after_deposit['total'] +
        Decimal('30.0') +
        Decimal('0.50') +
        Decimal('0.48')
    )
    assert cash_register_after_pay['visa'] == cash_register_after_deposit['visa'] + Decimal('30')
    assert cash_register_after_pay['interac'] == cash_register_after_deposit['interac'] + Decimal('0.50')
    assert cash_register_after_pay['cash'] == cash_register_after_deposit['cash'] + Decimal('0.48')

    # ---------- Refund the workorder (the 2$ deposit is refunded as cash) ----------
    methods_of_payment = {'visa': '30.00', 'interac': '0.50', 'cash': '2.48'}
    refund_items(client, methods_of_payment, workorder.id, [workorder.items[0].id, workorder.items[1].id])

    stats_after_refund = compute_workorder_stats(DATE_START, DATE_END)
    cash_register_after_refund = compute_cash_register_stats()

    # Back to the original stats (all items have been refunded, no profits are made)
    assert stats_after_refund == old_stats

    # The 2$ credit was paid cash and has been refunded as cash
    assert cash_register_after_refund['total'] == old_cash_register['total']
    assert cash_register_after_refund['cash'] == old_cash_register['cash']

    # The 2$ deposit has NOT been re-added to the client's account
    c = Client.get(workorder.client)
    assert c.in_store_credit == old_credit

def test_all_reports_cash_register_balance(client, login_admin, empty_db):
    from models import Workorder, Transaction
    from views import compute_workorder_stats, compute_cash_register_stats

    """
    Execute multiple tests back to back to check that the assertions
    remain consistent

    """
    old_stats = compute_workorder_stats(DATE_START, DATE_END)
    old_cash_register = compute_cash_register_stats()

    for key, value in old_stats.items():
        assert value == 0

    for key, value in old_cash_register.items():
        assert value == 0
    
    # ---------- Simple transaction ----------
    for method in ACCEPTED_PAYMENT_METHODS:
        workorder_id = create_workorder_with_two_items(client).id
        rv = client.post(f'/workorder/pay/{workorder_id}', data=dict(payment_method=method), follow_redirects=True)

        # Two items : 29.98 + tx = 32.98
        workorder = Workorder.get(workorder_id)
        assert workorder.paid_subtotal == Decimal('29.98')
        assert workorder.paid_total == Decimal('32.98')
        assert workorder.paid_total == workorder.paid_subtotal + workorder.paid_taxes1 + workorder.paid_taxes2
        assert workorder.paid

        assert Transaction.select().where(Transaction.workorder_id == workorder_id).count() == 1

    # ---------- Multiple transactions ----------
    workorder_id = create_workorder_with_two_items(client).id
    rv = client.post(f'/workorder/pay/{workorder_id}', data= {'visa': '32.00', 'interac': '0.50', 'cash': '0.48'}, follow_redirects=True)

    workorder = Workorder.get(workorder_id)
    assert workorder.paid

    # One transaction per payment method has been added
    assert Transaction.select().where(Transaction.workorder_id == workorder_id).count() == 3


    # ---------- Client credit deposit ----------
    workorder = create_workorder_with_two_items(client) 
    rv = client.post(f'/make-deposit/{workorder.id}', data={'amount' : '2.00', 'deposit_type': 'cash'}, follow_redirects=True)
    cash_register_after_client_deposit = compute_cash_register_stats()
    assert cash_register_after_client_deposit['credit account'] == Decimal('-2')
    assert cash_register_after_client_deposit['new_in_store_credit'] == Decimal('2')
    assert cash_register_after_client_deposit['used_in_store_credit'] == Decimal('0')

    rv = client.post(f'/workorder/pay/{workorder.id}', data= {'visa': '30.98', 'credit account': '2.00'}, follow_redirects=True)

    cash_register_after_pay = compute_cash_register_stats()
    stats_after_pay = compute_workorder_stats(DATE_START, DATE_END)

    # Checks if everything balances so far (32.98$ * 5 workorders = 164.90)
    assert stats_after_pay['total'] == Decimal('164.90')
    assert stats_after_pay['total'] == cash_register_after_pay['total'] + cash_register_after_pay['credit account']
    assert cash_register_after_pay['cash'] == Decimal('32.98') + Decimal('0.48') + Decimal('2.00') 
    assert cash_register_after_pay['interac'] == Decimal('32.98') + Decimal('0.50')
    assert cash_register_after_pay['visa'] == Decimal('32.98') + Decimal('32') + Decimal('30.98')

    assert cash_register_after_pay['credit account'] == Decimal('0')
    assert cash_register_after_pay['new_in_store_credit'] == Decimal('2')
    assert cash_register_after_pay['used_in_store_credit'] == Decimal('2')

    current_cash = cash_register_after_pay['cash']
    current_interac = cash_register_after_pay['interac']
    current_visa = cash_register_after_pay['visa']


    # ---------- Refund with cash ----------
    # Gets the first workorder
    workorder = Workorder.get(1)
    refund_items(client, dict(payment_method="cash"), workorder.id, [workorder.items[0].id, workorder.items[1].id])

    cash_register_after_cash_refund = compute_cash_register_stats()
    stats_after_cash_refund = compute_workorder_stats(DATE_START, DATE_END)
    
    # (164.90 - 32.98 = 131.92)
    assert stats_after_cash_refund['total'] == Decimal('131.92')
    assert stats_after_cash_refund['total'] == cash_register_after_cash_refund['total'] + cash_register_after_cash_refund['credit account']
    assert cash_register_after_cash_refund['cash'] == current_cash - Decimal('32.98')
    assert cash_register_after_cash_refund['interac'] == current_interac
    assert cash_register_after_cash_refund['visa'] == current_visa

    current_cash -= Decimal('32.98')


    # ---------- Refund with in_store_credit ----------
    # Gets the second workorder
    workorder = Workorder.get(2)
    old_in_store_credit = workorder.client.in_store_credit
    refund_items(client, dict(refund_as_in_store_credit="1"), workorder.id, [workorder.items[0].id, workorder.items[1].id])    

    cash_register_after_in_store_credit_refund = compute_cash_register_stats()
    stats_after_in_store_credit_refund = compute_workorder_stats(DATE_START, DATE_END)

    # (131.92 - 32.98 = 98.94)
    workorder = Workorder.get(2)
    new_in_store_credit = workorder.client.in_store_credit
    assert stats_after_in_store_credit_refund['total'] == Decimal('98.94')
    assert stats_after_in_store_credit_refund['total'] == cash_register_after_in_store_credit_refund['total'] + cash_register_after_in_store_credit_refund['credit account']
    assert cash_register_after_in_store_credit_refund['cash'] == current_cash
    assert cash_register_after_in_store_credit_refund['interac'] == current_interac
    assert cash_register_after_in_store_credit_refund['visa'] == current_visa

    # Total in-store credit received so far: 34.98. Total used so far: 2.00
    assert new_in_store_credit == old_in_store_credit + Decimal('32.98')
    assert cash_register_after_in_store_credit_refund['credit account'] == Decimal('-32.98')
    assert cash_register_after_in_store_credit_refund['new_in_store_credit'] == Decimal('34.98')
    assert cash_register_after_in_store_credit_refund['used_in_store_credit'] == Decimal('2')


    # ---------- Refund with all payment methods ----------
    # Gets the third workorder
    workorder = Workorder.get(3)
    refund_items(client, {'visa': '30.00', 'interac': '2.50', 'cash': '0.48'}, workorder.id, [workorder.items[0].id, workorder.items[1].id])

    cash_register_after_multiple_refund = compute_cash_register_stats()
    stats_after_multiple_refund = compute_workorder_stats(DATE_START, DATE_END)

    assert stats_after_multiple_refund['total'] == Decimal('65.96')
    assert stats_after_multiple_refund['total'] == cash_register_after_multiple_refund['total'] + cash_register_after_multiple_refund['credit account']
    assert cash_register_after_multiple_refund['cash'] == Decimal('2.00')
    assert cash_register_after_multiple_refund['interac'] == Decimal('30.98')
    assert cash_register_after_multiple_refund['visa'] == Decimal('65.96')
    assert cash_register_after_multiple_refund['credit account'] == Decimal('-32.98')


    # ---------- Refunds the rest as cash ----------
    # Gets the fourth and fifth workorders
    workorder = Workorder.get(4)
    refund_items(client, dict(payment_method="cash"), workorder.id, [workorder.items[0].id, workorder.items[1].id]) 
    workorder = Workorder.get(5)
    refund_items(client, dict(payment_method="cash"), workorder.id, [workorder.items[0].id, workorder.items[1].id]) 

    final_cash_register = compute_cash_register_stats()
    final_stats = compute_workorder_stats(DATE_START, DATE_END)

    assert final_stats['total'] == Decimal('0')
    assert final_stats['total'] == final_cash_register['total'] + final_cash_register['credit account']
    assert final_cash_register['total'] + final_cash_register['credit account'] == 0
    
    for key, value in final_stats.items():
        assert value == 0

    assert old_stats == final_stats

    """
TODO :

- Creating a workorder should not affect the cash register stats/the sales stats
- Add tests for the auto-complete searches
"""
