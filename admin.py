from setup_app import app, db
from models import *
from security import security

from flask_admin import Admin, BaseView, expose, AdminIndexView
from flask_admin.menu import MenuLink
from flask_admin.contrib.peewee import ModelView
from flask import request, jsonify, redirect, url_for

import json
from peewee import fn

app.config['FLASK_ADMIN_SWATCH'] = 'default'

class DashboardView(AdminIndexView):

    def is_visible(self):
        return False

admin = Admin(app, name='', template_mode='bootstrap4', index_view=DashboardView())

class HomeView(BaseView):
    @expose('/')
    def index(self):
        return redirect(url_for('index'))

class DatabaseCleanupView(BaseView):
    @expose('/')
    def index(self):
        from itertools import groupby

        def find_duplicates_query(field):
            """Returns clients with a duplicate field, ordered by that field"""
            return (Client.select()
                    .where(field.in_(
                        Client.select(field)
                        .where(fn.LENGTH(field) > 0)
                        .group_by(field)
                        .having(fn.COUNT(field) > 1)))
                    .order_by(field))

        # Requests all clients who have duplicate information

        # TODO : add ICU SQLite extension to use regex_replace() to keep only numbers
        client_dup_phone = find_duplicates_query(Client.phone)

        client_dup_email = find_duplicates_query(Client.email)

        full_name = Client.first_name + Client.last_name
        client_dup_name = (Client.select()
                                .where(full_name.in_(
                                   Client.select(full_name)
                                   .where((fn.LENGTH(Client.first_name) > 0) & (fn.LENGTH(Client.last_name) > 0))
                                   .group_by(full_name)
                                   .having(fn.COUNT(full_name) > 1)))
                                .order_by(full_name))

        # Groups together clients who have the same information
        phone_list = groupby(client_dup_phone, lambda x: x.phone)
        email_list = groupby(client_dup_email, lambda x: x.email)
        name_list = groupby(client_dup_name, lambda x: x.name())

        # Requests and section of erroneous info
        current_date = datetime.datetime.now()
        db_column = Client.first_name + Client.last_name + Client.address + Client.postal_code + Client.year_of_birth

        missing_names = Client.select().where((Client.first_name == '') | (Client.last_name == ''))
        suspicious_date = Client.select().where(Client.year_of_birth >= (current_date.year - 3))
        wrong_character = Client.select().where(db_column.contains("@"))

        return self.render('admin/database-cleanup.html',
                           client_dup_phone=phone_list,
                           client_dup_email=email_list,
                           client_dup_name=name_list,
                           missing_names=missing_names,
                           suspicious_date=suspicious_date,
                           wrong_character=wrong_character)

class CustomizeQuickFindButtonsView(BaseView):
    @expose('/')
    def index(self):

        quick_items = (InventoryItem.select(InventoryItem.category, InventoryItem.subcategory)
                                    .where(
                                        (InventoryItem.category != '') &
                                        (InventoryItem.subcategory != '')
                                    ).group_by(InventoryItem.category, InventoryItem.subcategory)
                                     .order_by(InventoryItem.category)
                                     .dicts())

        quick_buttons = QuickButton.select().order_by(QuickButton.display_order).dicts()


        return self.render('admin/quick-find.html',
                            items=quick_items,
                            buttons=quick_buttons)

    @expose('/api/quickmenu', methods=['POST'])
    def api_quickmenu(self):
        form = request.form

        if 'ids' in form:
            data = json.loads(form['ids'])
            button_ids = data['button_ids']
            buttons = []

            QuickButton.update({QuickButton.display_order: None}).execute()

            order = 0
            for button_id in button_ids:
                button = QuickButton.get(button_id)
                button.display_order = order
                buttons.append(button)
                order += 1

            QuickButton.bulk_update(buttons, fields=[QuickButton.display_order])

            return jsonify(True)

        if 'delete_id' in form:
            order = QuickButton.get(QuickButton.id==form['delete_id']).display_order
            QuickButton.delete().where(QuickButton.id==form['delete_id']).execute()

            quickbuttons = QuickButton.select() \
                                      .where(QuickButton.display_order > order) \
                                      .order_by(QuickButton.display_order.asc())

            buttons = []
            for button in quickbuttons:
                button.display_order = button.display_order - 1
                buttons.append(button)

            if len(buttons) > 0:
                QuickButton.bulk_update(buttons, fields=[QuickButton.display_order])

            count = QuickButton.select().count()

            return {'count' : count}

        if 'id' in form:
            button = QuickButton.get(QuickButton.id==form['id'])
        else:
            button = QuickButton()
            button.category = form['category']
            button.subcategory = form['subcategory']

            button.display_order = QuickButton.select().count()

        button.name = form['name']
        button.save()

        return button.serialized()

class CustomizeWorkorderStatusView(BaseView):
    @expose('/api/status/', methods=['POST'])
    def api_status(self):
        """ Modifies or creates a workorder status, and sets the display
        order of the statuses.

        It is possible to set a status to archive using GET,
         if the status_id is provided as a parameter. """

        form = request.form

        # Rearranging order
        if 'ids' in form:
            data = json.loads(form['ids'])
            status_ids = data['status_ids']
            statuses = []

            WorkorderStatus.update({WorkorderStatus.display_order:None}).execute()

            order = 0
            for status_id in status_ids:
                status = WorkorderStatus.get(status_id)
                status.display_order = order
                statuses.append(status)
                order += 1

            WorkorderStatus.bulk_update(statuses, fields=[WorkorderStatus.display_order])

            return jsonify(True)

        # Archiving
        if 'archived' in form:
            
            if form['archived'] == 'true':
                status = WorkorderStatus.get(WorkorderStatus.id==form['statusId'])

                order = status.display_order
                status.display_order = None
                status.archived = True
                status.save()

                workorderstatuses = WorkorderStatus.select() \
                                                .where(WorkorderStatus.display_order > order) \
                                                .order_by(WorkorderStatus.display_order.asc())
                statuses = []
                for status in workorderstatuses:
                    status.display_order = status.display_order - 1
                    statuses.append(status)

                if len(statuses) > 0:
                    WorkorderStatus.bulk_update(statuses, fields=[WorkorderStatus.display_order])

                # This should always be true, if it's not, there's a flaw in the logic
                nb_not_archived = WorkorderStatus.select().where(~WorkorderStatus.archived).count()
                orders = set(w.display_order for w in WorkorderStatus.select().where(~WorkorderStatus.archived))
                assert orders == set(range(nb_not_archived))
            else:
                status = WorkorderStatus.get(WorkorderStatus.id == form['statusId'])
                last_place = WorkorderStatus.select(fn.MAX(WorkorderStatus.display_order)).scalar()
                status.display_order = last_place + 1
                status.archived = False
                status.save()

                return status.serialized()

            return jsonify(True)


        # Modifiying/Creating
        if 'id' in form:
            status = WorkorderStatus.get(WorkorderStatus.id==form['id'])
        else:
            last_place = WorkorderStatus.select(fn.MAX(WorkorderStatus.display_order)).scalar()

            if last_place is None: # All statuses are archived
                last_place = -1

            status = WorkorderStatus()
            status.display_order = last_place + 1

        if status.id != WorkorderStatus.OPEN_ID and status.id != WorkorderStatus.CLOSED_ID:
            status.name = form['name']

        status.color = form['color']
        status.save()

        return status.serialized()



    @expose('/')
    def index(self):
        workorderstatuses = []
        statuses = WorkorderStatus.select().where(
            (WorkorderStatus.archived == False) &
            (WorkorderStatus.display_order == None))

        for status in statuses:
            last_place = WorkorderStatus.select(fn.MAX(WorkorderStatus.display_order)).scalar()
            status.display_order = last_place + 1
            status.save()

        statuses = WorkorderStatus.select() \
                                  .where(WorkorderStatus.archived == False) \
                                  .order_by(WorkorderStatus.display_order)

        archived_statuses = WorkorderStatus.select() \
                                           .where(WorkorderStatus.archived)

        for status in statuses:
            workorderstatuses.append(status.serialized())

        special_statuses = [WorkorderStatus.OPEN_ID, WorkorderStatus.CLOSED_ID]
        return self.render('admin/workorderstatus.html',
                             statuses=statuses,
                             workorderstatuses=workorderstatuses,
                             special_ids=special_statuses,
                             archived_statuses=archived_statuses)


admin.add_view(HomeView(name='Back to app', endpoint='homeview'))
admin.add_view(CustomizeQuickFindButtonsView(name='Cutomize Quick Find Menu', endpoint='quick-find', category='Customization'))
admin.add_view(CustomizeWorkorderStatusView(name='Cutomize Workorder Status', endpoint='status', category='Customization'))
admin.add_view(DatabaseCleanupView(name='Database cleanup', endpoint='database-cleanup'))


class ClientModelView(ModelView):
    column_searchable_list = ['first_name', 'last_name', 'address', 'phone', 'email', 'internal_notes']
    column_list = ['archived', 'first_name', 'last_name', 'postal_code', 'phone', 'email', 'created']
    column_filters = ['postal_code', 'created', 'internal_notes', 'year_of_birth', 'email_consent']
    page_size = 80
    column_default_sort = [('archived', False), ('id', False)]
    can_export = True
    can_view_details = True
    details_modal = True

class InventoryItemModelView(ModelView):
    column_searchable_list = ['name', 'keywords', 'category', 'subcategory', 'item_codes']
    column_filters = ['name', 'price', 'avg_cost', 'current_cost', 'type']
    column_list = ['id', 'archived', 'name', 'price', 'avg_cost', 'current_cost', 'category', 'subcategory', 'item_codes']
    column_default_sort = [('archived', False), ('id', False)]
    page_size = 80
    can_export = True
    can_view_details = True
    details_modal = True

class WorkorderModelView(ModelView):
    inline_models = (WorkorderItem,)
    column_list = ['client', 'paid', 'status', 'bike_description', 'invoice_notes', 'internal_notes', 'type', 'created']
    column_default_sort = [('created', True)]
    can_create = False
    can_export = True
    can_view_details = True
    details_modal = True

class WorkorderStatusModelView(ModelView):
    column_searchable_list = ['name', 'color']
    column_filters = ['name', 'color', 'display_order', 'archived']
    column_list = ['id', 'archived', 'name', 'color', 'display_order']
    column_default_sort = [('archived', False), ('display_order', False), ('id', False)]
    page_size = 80
    can_export = True
    can_view_details = True
    details_modal = True

class TransactionModelView(ModelView):
    column_searchable_list = ['payment_method', 'comment']
    column_filters = ['created']
    named_filter_urls = True
    column_default_sort = [('created', True)]
    page_size = 80
    can_export = True
    can_create = False
    can_edit = False
    can_delete = False
    can_view_details = True
    details_modal = True

class CashRegisterStateModelView(ModelView):
    # column_searchable_list = ['payment_method', 'comment']
    column_filters = ['created']
    column_default_sort = [('created', True)]
    page_size = 80
    can_export = True
    can_create = False
    can_edit = False
    can_delete = False
    can_view_details = True
    details_modal = True

class InventoryPurchaseModelView(ModelView):
    # column_searchable_list = ['payment_method', 'comment']
    column_filters = ['created']
    column_default_sort = [('created', True)]
    page_size = 80
    can_export = True
    can_create = False
    can_edit = False
    can_delete = False
    can_view_details = True
    details_modal = True

class InventoryHistoryModelView(ModelView):
    # column_searchable_list = ['payment_method', 'comment']
    column_filters = ['event', 'time']
    column_default_sort = [('time', True)]
    page_size = 80
    can_export = True
    can_create = False
    can_edit = False
    can_delete = False
    can_view_details = True
    details_modal = True


admin.add_view(ClientModelView(Client, menu_icon_type='fa', menu_icon_value='fa-user', category='Raw data'))
admin.add_view(InventoryItemModelView(InventoryItem, menu_icon_type='fa', menu_icon_value='fa-cog', category='Raw data'))
admin.add_view(WorkorderModelView(Workorder, menu_icon_type='fa', menu_icon_value='fa-list-alt', category='Raw data'))
admin.add_view(WorkorderStatusModelView(WorkorderStatus, menu_icon_type='fa', menu_icon_value='fa-check-square', category='Raw data'))
admin.add_view(ModelView(WorkorderItem, menu_icon_type='fa', menu_icon_value='fa-list', category='Raw data'))
admin.add_view(TransactionModelView(Transaction, menu_icon_type='fa', menu_icon_value='fa-usd', category='Raw data'))
admin.add_view(CashRegisterStateModelView(CashRegisterState, menu_icon_type='fa', menu_icon_value='fa-money', category='Raw data'))
admin.add_view(InventoryPurchaseModelView(InventoryPurchase, menu_icon_type='fa', menu_icon_value='fa-truck-loading', category='Raw data'))
admin.add_view(InventoryHistoryModelView(InventoryHistory, menu_icon_type='fa', menu_icon_value='fa-boxes', category='Raw data'))

from flask_admin import helpers as admin_helpers
@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
    )
