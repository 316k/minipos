# Login

- [ ] Connection 

# Home page

- [ ] Create new client
- [ ] Create a direct sale
- [ ] Create a new workorder with a existant client
- [ ] Create a workorder with a bike tag
- [ ] Create a workroder with a new volunteer project tag
- [ ] View recent paid workorders
- [ ] View unclosed workorders

# Client creation

## Simple Client

- [ ] Create a new client
- [ ] Fill all fields correctly
- [ ] Submit : there should be no error

## Erroneous client

- [ ] Create a new client
- [ ] Fill all the fields correctly except for `last_name`
- [ ] Submit : there should be an error preventing the form from being submitted
- [ ] Fill the missing input : the error should be hidden

Test two times more, instead with the missing fields : `email` and `phone`

# Receipts

## Emails : no js here

## Receipts

- [ ] Clicking on `Print receipt` should automatically trigger the Print Dialog
- [ ] Cancelling and clicking on the `Print` button should trigger the Print Dialog

# Workorders

<!-- ## Workorders with a client 

- [ ] Delete the workorder
- [ ] Change the client
- [ ] Change the workorder to a direct sale (with no client)
- [ ] Edit the information of the workorder(bike information)
- [ ] See the total
- [ ] Print a receipt without paying
- [ ] Pay the workorder if there is some items (cash, debit, visa)
- [ ] Print a receipt after paying
- [ ] Send a email receipt if client is consent 
- [ ] Refund the workorder if its paid
- [ ] Search for items 
- [ ] Add a item to the workorder 
- [ ] Delete a item from the workorder
- [ ] Edit a item for the workorder  -->

## Delete workorder without items
- [ ] Create a workorder
- [ ] Delete the workorder

## Delete workorder with items
- [ ] Create a workorder
- [ ] Add a item to the workorder 
- [ ] Delete the workorder
- [ ] Confirm the notificaion before deleting

## Pay workorder cash
- [ ] Create a workorder
- [ ] Add a item to the workorder and change the quantity to 2
- [ ] Pay cash
- [ ] Enter 200$ more then the total of workorder and verify that the change is 200$
- [ ] Finish the payment 

## Pay workorder debit
- [ ] Create a workorder
- [ ] Add a item to the workorder and change the quantity to 2
- [ ] Pay with debit
- [ ] Enter the amount of total in the terminal
- [ ] Finish the payment

## Pay workorder visa
- [ ] Create a workorder
- [ ] Add a item to the workorder and change the quantity to 2
- [ ] Pay with visa
- [ ] Enter the amount of total in the terminal
- [ ] Finish the payment

## Editing the client
- [ ] Create a direct sale
- [ ] Set the client to a new client
- [ ] Create the client and come back to the workorder
- [ ] Set the client to an existing one
- [ ] Set the client back to Direct sale

<!-- ## Delete a workorder
- [ ] Create a workorder 
- [ ] Delete workorder
    ## If workorder have items
    - [ ] Confirm the notificaion before deleting -->

<!-- ## Direct Sale (Workorder without a client)

- [ ] Delete the workorder
- [ ] Set a client
- [ ] Edit the information of the workorder (bike information)
- [ ] See the total
- [ ] Print a receipt without paying
- [ ] Pay the workorder if there is some items (cash, debit, visa)
- [ ] Print a receipt after paying
- [ ] Send a email receipt
- [ ] Refund the workorder if its paid
- [ ] Search for items 
- [ ] Add a item to the workorder 
- [ ] Delete a item from the workorder
- [ ] Edit a item for the workorder  -->

# Reports

TODO: various charts

# Admin

TODO: workorderstatuses and quick-add buttons administration
