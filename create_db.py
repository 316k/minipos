#!/usr/bin/env python3
from models import *
from import_db import *
import config

def create_db(import_data=False):
    # ---------- Setup database ----------
    Client.add_index(peewee.SQL('CREATE INDEX client_idx_first_name ON client(first_name COLLATE NOCASE)'))
    Client.add_index(peewee.SQL('CREATE INDEX client_idx_last_name ON client(last_name COLLATE NOCASE)'))

    InventoryItem.add_index(peewee.SQL('CREATE INDEX inventoryitem_idx_name ON inventoryitem(name COLLATE NOCASE)'))

    for Model in (Role, User, UserRoles, LightRole,
                  Client, InventoryItem, InventoryPurchase, InventoryHistory,
                  WorkorderStatus, Workorder, WorkorderItem,
                  Transaction, CashRegisterState, QuickButton):
        Model.create_table()

    # Required data
    LightRole.insert_many([
        ('admin', config.admin_access_code),
        ('employee', config.employee_access_code),
    ]).execute()

    WorkorderStatus.insert_many([
        (1, 'Open', '#70AAFF', 0),
        (2, 'Closed', '#BBBBBB', 1),
    ], fields=(WorkorderStatus.id,
               WorkorderStatus.name,
               WorkorderStatus.color,
               WorkorderStatus.display_order
               )).execute()

    CashRegisterState.insert(
        expected_cash=Decimal('0.00'),
        expected_visa=Decimal('0.00'),
        expected_interac=Decimal('0.00'),
        confirmed_cash=Decimal('0.00'),
        confirmed_visa=Decimal('0.00'),
        confirmed_interac=Decimal('0.00'),
        cash_fund=Decimal('0.00'),
        comment='Database created',
        open=True,
    ).execute()


    # Importation
    if import_data:
        import_all()

    # Example/test data
    else:
        Client.insert_many([
            ('Jimmy', 'Whooper', '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'jimmy@example.com', 1, ''),
            ('Alice', 'Whooper', '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'alice@example.com', 0, ''),
            ('Theresa', 'Horton', '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'theresa@example.com', 0, ''),
            ('Robert', 'Tackitt',  '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'robert@example.com', 0, ''),
            ('Helen', 'Nathan',  '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'helen@example.com', 0, ''),
            ('Robin', 'Graves',  '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'robin@example.com', 0, ''),
            ('Carolyn', 'Degraffenreid',  '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'carolyn@example.com', 0, ''),
            ('Katelyn', 'Anderson',  '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'katelyn@example.com', 0, ''),
            ('Louise', 'Howard',  '123 rue Jarry', 'H2R 1M8', '514 222 3333', 'louise@example.com', 0, ''),
        ], fields=(Client.first_name,
                   Client.last_name,
                   Client.address,
                   Client.postal_code,
                   Client.phone,
                   Client.email,
                   Client.email_consent,
                   Client.internal_notes)).execute()

        # XXX : This specific item is used in ui_test.py
        InventoryItem.insert(name='Abonnement Simple', keywords='membership abonnement annuel membre diy atelier',
                             price=4.99, avg_cost=0.0, current_cost=0.0, msrp=0, taxable=True, avg_time_spent=0,
                             special_meaning='membership').execute()

        InventoryItem.insert_many([
            ('Mise au point standard', 'mapbb checkup', 45.00, 0, 0, 0, 45, "Main d'oeuvre", 'Mise au point', 0, ''),
            ('Mise au point avancée', 'checkup complet', 75.00, 0, 0, 0, 90, "Main d'oeuvre", 'Mise au point', 0, 'NH: dont 1 a LucienPagé 22 janv 2019 NH: 2 manquant (24.01.2019) quantité ajustée 1 manquant 20/11/2019 NH'),
            ('Mise au point standard', 'mapbb checkup', 55.00, 0, 0, 0, 45, "Main d'oeuvre", 'Mise au point', 1),
            ('Mise au point avancée', 'checkup complet', 25.00, 0, 0, 0, 90, "Main d'oeuvre", 'Mise au point', 1),
            ('Mise au point hiver standard', 'checkup', 95.00, 0, 0, 0, 80, "Main d'oeuvre", 'Mise au point', 0),
            ('Mise au point hiver avancée', 'checkup complet', 145.00, 0, 0, 0, 120, "Main d'oeuvre", 'Mise au point', 0),
            # XXX : This specific item is used in ui_test.py
            ('Chambre à air + Installation', 'flat crevaison pneu crevé', 12.00, 3, 3, 0, 10, "", '', 0),
            ('Chambre à air', 'flat crevaison pneu crevé', 6.00, 1.00, 1.00, 0, 0, "", '', 0),
            ('Heure Atelier DIY', 'self-service', 1.00, 0, 0, 0, 0, "", '', 0),
            ('Pneu usagé', 'used tire', 24.00, 1.00, 1.00, 0, 0, "", '', 0),
            ('Pneu usagé', 'used tire', 28.00, 0, 0, 0, 0, "", '', 1),
            ('Pneu usagé (hiver)', 'used tire winter', 35.00, 0, 0, 0, 0, "", '', 0),
            ('Pneu usagé (hiver)', 'used tire winter', 25.00, 0, 0, 0, 0, "", '', 1),
            ('Patins de freins usagés', '', 8.00, '3.00', '3.00', 0, 0, "Pièces", 'Freins', 0),
            ('Patins de freins neufs', '', 16.00, 6, 0, 0, 0, "Pièces", 'Freins', 0),
            ('Patins de freins neufs', '', 26.00, 6, 0, 0, 0, "Pièces", 'Freins', 1),
        ], fields=(
            InventoryItem.name, InventoryItem.keywords,
            InventoryItem.price, InventoryItem.avg_cost, InventoryItem.current_cost, InventoryItem.msrp,
            InventoryItem.avg_time_spent,
            InventoryItem.category, InventoryItem.subcategory,
            InventoryItem.archived, InventoryItem.note)).execute()

        InventoryItem.insert_many([
            (f"Test item that will overflow the search box {i}", '', '1.99', '1.00', '1.00', 0, 45, "Test", 'Test', 10, True)
            for i in range(30)
        ], fields=(
            InventoryItem.name, InventoryItem.keywords,
            InventoryItem.price, InventoryItem.avg_cost, InventoryItem.current_cost, InventoryItem.msrp,
            InventoryItem.avg_time_spent,
            InventoryItem.category, InventoryItem.subcategory,
            InventoryItem.current_inventory_count,
            InventoryItem.keep_track_in_inventory,
        )).execute()

        InventoryItem.insert(name='Don', keywords='donate donation',
                             category='don', subcategory='',
                             price=10.00, avg_cost=0, current_cost=0,
                             msrp=0, always_edit=True, taxable=False,
                             avg_time_spent=0,
                             special_meaning='donation').execute()

        InventoryItem.insert(
            name="* boulons bolts M5 x 25mm inox stainless (25pcs) whl-mfg",
            price="17.99", avg_cost="9.99", current_cost="9.99", msrp="17.99",
            item_codes=["M5-25SHSS","836572003626"],
            current_inventory_count=50, keep_track_in_inventory=True,
            taxable=True, avg_time_spent=0).execute()

        InventoryItem.insert(
            name="Pneu 26 x 1.9 Vee Rubber NIMBUS VRB-159",
            price="19.99", avg_cost="10.00", current_cost="10.00", msrp="19.99",
            item_codes=["26X1.90VRB159 BK","848712018094"],
            current_inventory_count=50, keep_track_in_inventory=True,
            taxable=True, avg_time_spent=0).execute()

        # XXX : This specific item is used in tests
        InventoryItem.insert(name='Abonnement Premium', keywords='membership abonnement annuel membre diy atelier',
                             price=24.99, avg_cost=0.0, current_cost=0.0, msrp=0,
                             taxable=True, avg_time_spent=0,
                             special_meaning='membership').execute()

        # Special item : "custom" to add an arbitrary item to the sale
        InventoryItem(
            name="Custom item",
            category="custom",
            subcategory="item",
            item_codes=[],
            price='0',
            avg_cost='0',
            current_cost='0',
            msrp='0',
            taxable=True,
            discountable=False,
            type='other',
            always_edit=True,
            special_meaning='custom').save()

        WorkorderStatus.insert_many([
            ('RDV MAP', '#8DB6D7', 2),
            ('À évaluer', '#fc00c9', 3),
            ('Approuvé', '#ae00ff', 4),
            ('RDV Pickup', '#8DB6D7', 5),
            ('Done', '#636363', 6),
            ('À vendre', '#8DB6D7', 7),
            ('En cours', '#0D6EFD', 8),
        ], fields=(WorkorderStatus.name, WorkorderStatus.color,
                   WorkorderStatus.display_order)
                                    ).execute()

        one_day_ago = datetime.datetime.now() - datetime.timedelta(days=1)
        four_hours_ago = datetime.datetime.now() - datetime.timedelta(hours=4)
        two_hours_ago = datetime.datetime.now() - datetime.timedelta(hours=2)
        one_hour_ago = datetime.datetime.now() - datetime.timedelta(hours=1)

        # Coherent example Cash Register
        register_state = CashRegisterState.get()
        register_state.created = one_day_ago
        register_state.save()

        # Example paid workorders
        Workorder.insert_many([
            (1, 1, 'Nakamura red', 'Basic tuneup', 'Internal notes here',
             four_hours_ago, four_hours_ago, four_hours_ago, one_hour_ago,
             WorkorderStatus.CLOSED_ID, '1.00', '123.00', '0.05', '0.09975', '6.15', '12.27', '141.42'),
            (2, None, '', '', '',
             one_day_ago, one_day_ago, one_day_ago, two_hours_ago,
             WorkorderStatus.CLOSED_ID, '3.00', '5.00', '0.05', '0.09975', '0', '0', '5.00'),
        ], fields=(
            Workorder.id,
            Workorder.client_id, Workorder.bike_description,
            Workorder.invoice_notes, Workorder.internal_notes,
            Workorder.calendar_date, Workorder.created, Workorder.updated,
            Workorder.paid_date,
            Workorder.status_id,
            Workorder.paid_cost,
            Workorder.paid_subtotal, Workorder.paid_tax1_rate,
            Workorder.paid_tax2_rate, Workorder.paid_taxes1,
            Workorder.paid_taxes2, Workorder.paid_total)).execute()

        # Example unpaid workorders
        Workorder.insert_many([
            (3, 1, 'Big fatbike', 'Winter tuneup',
             'Winter Tuneup\nI already checked the chain, all seems good\nStill waiting for bearings to ship',
             four_hours_ago, four_hours_ago, four_hours_ago),
            (4, 2, 'Red Spider-man kid''s bike', 'Remove training wheels',
             '',
             one_hour_ago, one_hour_ago, one_hour_ago),
            (5, None, '', '', ''),
        ], fields=(
            Workorder.id, Workorder.client_id,
            Workorder.bike_description,
            Workorder.invoice_notes, Workorder.internal_notes,
            Workorder.calendar_date, Workorder.created, Workorder.updated,
        )).execute()

        Transaction.insert_many([
            ('141.42', 'interac', 1, one_hour_ago),
            ('5.00', 'cash', 2, two_hours_ago),
        ], fields=(Transaction.amount, Transaction.payment_method, Transaction.workorder_id, Transaction.created)).execute()

        WorkorderItem.create_table()

        WorkorderItem.insert_many([
            (1, 1, 'Abonnement Simple', '24.00', '0.00', '24.00', True),
            (1, 3, 'Mise au point avancée', '75.00', '0.00', '75.00', True),
            (1, 9, 'Pneu usagé', '24.00', '1.00', '24.00', True),
            (2, 1, 'Patins de freins usagé', '5.00', '3.00', '8.00', False),
            (3, 3, 'Mise au point avancée', '75.00', '0.00', '75.00', True),
            (5, 3, 'Mise au point avancée', '75.00', '0.00', '75.00', True),
        ], fields=(WorkorderItem.workorder_id, WorkorderItem.inventory_item_id,
                   WorkorderItem.name, WorkorderItem.price, WorkorderItem.cost,
                   WorkorderItem.orig_price, WorkorderItem.taxable)).execute()

        # Memberships
        client = Client.get(1)
        client.membership_paid_until = one_hour_ago + datetime.timedelta(days=365)
        client.membership_item_id = 1
        client.save()

        # Example quick buttons
        QuickButton.insert(
            name='MAP',
            category="Main d'oeuvre",
            subcategory='Mise au point',
            display_order=None
        ).execute()
        QuickButton.insert(
            name='Freins',
            category="Pièces",
            subcategory='Freins',
            display_order=None
        ).execute()

        QuickButton.insert(
            name='Don',
            category="don",
            subcategory='',
            display_order=None
        ).execute()

        QuickButton.insert(
            name='Custom',
            category="custom",
            subcategory='item',
            display_order=None
        ).execute()


def anonymize():
    from random import seed, choice, random
    import string

    seed(1337)

    def randomize_char(c, multiple=True):

        # Randomly add similar chars
        times = 1 + (multiple and random() < 0.1)

        if c in string.ascii_lowercase:
            return ''.join(choice(string.ascii_lowercase) for i in range(times))
        elif c in string.ascii_uppercase:
            return ''.join(choice(string.ascii_uppercase) for i in range(times))
        elif c in string.digits:
            return ''.join(choice(string.digits) for i in range(times))

        return c

    example_names = []
    with open('data/prenoms.txt') as f:
        for name in f:
            example_names.append(string.capwords(name))

    for client in Client.select():

        if client.first_name:
            parts = len(client.first_name.split())
            new_name = choice(example_names)
            while len(new_name.split()) != parts:
                new_name = choice(example_names)
            client.first_name = new_name

        if client.last_name:
            parts = len(client.last_name.split())
            new_name = choice(example_names)
            while len(new_name.split()) != parts:
                new_name = choice(example_names)

            client.last_name = new_name

        addr = ''
        for c in client.address:
            addr += randomize_char(c)

        client.address = addr

        if client.postal_code:
            client.postal_code = choice(('H0H 0H0', 'H1A 0H0', 'H1B 0H0'))

        phone = ''
        for c in client.phone:
            phone += randomize_char(c, multiple=False)
        client.phone = phone

        email = ''
        for c in client.email:
            email += randomize_char(c, multiple=True)
        client.email = email

        internal_notes = ''
        for c in client.internal_notes:
            internal_notes += randomize_char(c)
        client.internal_notes = internal_notes

        client.save()

    for workorder in Workorder.select() \
                              .where((Workorder.bike_serial_number != '') |
                                     (Workorder.internal_notes != '') |
                                     (Workorder.invoice_notes != '')):

        bike_serial_number = ''
        for c in workorder.bike_serial_number:
            bike_serial_number += randomize_char(c)
        workorder.bike_serial_number = bike_serial_number

        internal_notes = ''
        for c in workorder.internal_notes:
            internal_notes += randomize_char(c)
        workorder.internal_notes = internal_notes

        invoice_notes = ''
        for c in workorder.invoice_notes:
            invoice_notes += randomize_char(c)
        workorder.invoice_notes = invoice_notes

        workorder.save()

    # Anonymize imported labels/custom items, as they may contain
    # phone numbers from invoice notes
    for item in WorkorderItem.select() \
                             .where(WorkorderItem.inventory_item_id.in_(
                                 InventoryItem.select(InventoryItem.id).where(InventoryItem.special_meaning != '')
                             )):

        name = ''

        if item.name.startswith('Work order #'):
            item.name = item.name[len('Work order #'):]
            name = 'Work order #'

        for c in item.name:
            name += randomize_char(c)
        item.name = name

        item.save()
