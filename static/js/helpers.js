function format_currency(price) {
    return '$' + (Math.round(price * 100) / 100).toFixed(2);
}

Handlebars.registerHelper("format_currency", function(text, options) {
    let escapedText = +text;

    let output = format_currency(escapedText);

    return new Handlebars.SafeString(output);
});

// https://stackoverflow.com/a/326076/14639652
function in_iframe() {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

// Refresh the page when the back button is pressed to avoid old data to
// be shown
//
// https://stackoverflow.com/a/43043658/14639652
window.addEventListener( "pageshow", function ( event ) {
  var historyTraversal = event.persisted ||
                         ( typeof window.performance != "undefined" &&
                              window.performance.navigation.type === 2 );
  if ( historyTraversal ) {
    // Handle page restore.
    window.location.reload();
  }
});

// Auto redirect to /login when logged out (eg.: long inactivity,
// logged out from a different page, etc)
document.addEventListener('DOMContentLoaded', function() {
    if(document.body.classList.contains('is-authenticated')) {
        let interval = setInterval(function() {
            $.get('/api/logged_in', function(data) {
                if(!data) {
                    window.location.reload();
                }
            }).fail(function() {
                window.location.reload();
            });
        }, 30000); // Check every 30 seconds
    }
});
