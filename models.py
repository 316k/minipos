import os
import peewee
# from flask_peewee.db import Database
import datetime
from playhouse.hybrid import hybrid_property
from decimal import Decimal
import config
from playhouse.sqlite_ext import JSONField

from setup_app import db

import decimal

decimal.getcontext().rounding = 'ROUND_HALF_UP'

# ---------- Flask-Security ----------
# https://github.com/flask-admin/flask-admin/blob/master/examples/auth/app.py
# https://flask-security-too.readthedocs.io/en/stable/quickstart.html#basic-peewee-application
from flask_security import Security, PeeweeUserDatastore, \
    UserMixin, RoleMixin, auth_required, hash_password, current_user

class Role(RoleMixin, db.Model):
    name = peewee.CharField(unique=True)
    description = peewee.TextField(null=True)
    permissions = peewee.TextField(null=True)

# N.B. order is important since Model also contains a get_id() -
# we need the one from UserMixin.
class User(UserMixin, db.Model):
    email = peewee.TextField()
    password = peewee.TextField()
    active = peewee.BooleanField(default=True)
    fs_uniquifier = peewee.TextField(null=False)
    confirmed_at = peewee.DateTimeField(null=True)
    last_login_at = peewee.DateTimeField(null=True)
    current_login_at = peewee.DateTimeField(null=True)
    last_login_ip = peewee.TextField(null=True)
    current_login_ip = peewee.TextField(null=True)
    login_count = peewee.IntegerField(default=0)

class UserRoles(db.Model):
    # Because peewee does not come with built-in many-to-many
    # relationships, we need this intermediary class to link
    # user to roles.
    user = peewee.ForeignKeyField(User, backref='roles')
    role = peewee.ForeignKeyField(Role, backref='users')
    name = property(lambda self: self.role.name)
    description = property(lambda self: self.role.description)

    def get_permissions(self):
        return self.role.get_permissions()

class LightRole(db.Model):
    """Lightweight role, separate from the Flask-Security roles. See
       security.py for more details.

    """
    name = peewee.CharField(unique=True)
    password = peewee.CharField(unique=True)

# ---------- Models ----------

current_dir = os.path.dirname(os.path.abspath(__file__))
postal_codes = {}

with open(current_dir + '/data/postal_codes.txt') as f:
    for line in f:
        postal_codes[line[:3]] = line[4:].strip()

class InventoryItem(db.Model):
    # SQLite explicit AUTOINCREMENT == don't reuse a previously delete ID
    id = peewee.BigIntegerField(primary_key=True, constraints=[peewee.SQL('AUTOINCREMENT')])
    name = peewee.TextField()

    # Keywords are part of the search without being part of the
    # displayed name (eg.: extra synonyms/translations : name: "Steel
    # wheel", keywords: "Roue acier")
    keywords = peewee.TextField(default='')

    category = peewee.TextField(default='')
    subcategory = peewee.TextField(default='')

    # SKU, EAN, and all other vendor codes stored in a JSON object
    # {"sku": "...", "ean": "...", ...}
    item_codes = JSONField(default="{}")

    price = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    msrp = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)

    # -- Cost fields --
    # DO NOT UPDATE DIRECTLY, update through InventoryHistory's @classmethods
    #
    # The average cost of this item (see: https://en.wikipedia.org/wiki/Average_cost_method)
    avg_cost = peewee.DecimalField(max_digits=15, decimal_places=4, auto_round=True)
    # The most recent cost for this item, NOT the cost of the next
    # item that will be sold to a client. This cost should only be
    # used :
    # - As a suggestion when purchasing new inventory
    # - As a price guide when choosing the right discount for an item
    current_cost = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)

    taxable = peewee.BooleanField(default=True)
    discountable = peewee.BooleanField(default=True)
    # price_with_discount INTEGER NULLABLE DEFAULT NULL,

    # Approximate time spent (in minutes), for stats purposes
    avg_time_spent = peewee.IntegerField(default=0)

    # Allows to find items that require special treatment, such as
    # membership tiers
    special_meaning = peewee.TextField(default='')
    # Some items always need to be edited (eg.: a donation doesn't
    # have a fixed amount)
    always_edit = peewee.BooleanField(default=False)


    type = peewee.TextField(default='article', choices=((k, k) for k in ('labor', 'article', 'other')),
                            constraints=[peewee.SQL('''CHECK("type" IN ('labor', 'article', 'other'))''')])

    keep_track_in_inventory = peewee.BooleanField(default=False)
    # DO NOT UPDATE DIRECTLY, update through InventoryHistory's @classmethods
    current_inventory_count = peewee.IntegerField(default=0)

    note = peewee.TextField(null=True)
    archived = peewee.BooleanField(default=False)

    def __str__(self):
        return self.name

class Client(db.Model):
    # Important : with peewee, the id primary key doesn't have to be
    # explicitly specified. However, this is done explicitly in a few
    # models here to add the AUTOINCREMENT keywork.
    #
    # >  4. If the AUTOINCREMENT keyword appears after INTEGER PRIMARY
    # >  KEY, that changes the automatic ROWID assignment algorithm to
    # >  prevent the reuse of ROWIDs over the lifetime of the database. In
    # >  other words, the purpose of AUTOINCREMENT is to prevent the reuse
    # >  of ROWIDs from previously deleted rows.
    #
    # - https://sqlite.org/autoinc.html
    #
    # TL;DR : SQLite explicit AUTOINCREMENT == don't reuse a previously delete ID
    id = peewee.BigIntegerField(primary_key=True, constraints=[peewee.SQL('AUTOINCREMENT')])

    first_name = peewee.TextField()
    last_name = peewee.TextField()
    address = peewee.TextField(default='')
    postal_code = peewee.TextField(default='')
    phone = peewee.TextField(default='')
    email = peewee.TextField(default='')
    email_consent = peewee.BooleanField(default=False)
    year_of_birth = peewee.IntegerField(null=True, default=None)
    internal_notes = peewee.TextField(default='')

    in_store_credit = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True, default=0)

    # FIXME : find a way to factor out shop-specific code from the
    # application. Membership is not a universal need, nor is it
    # always treated the same way
    #
    # A possible way could be to have a json_object custom_data that
    # stores whatever this instance of the app requires
    membership_paid_until = peewee.DateTimeField(null=True)
    membership_item = peewee.ForeignKeyField(InventoryItem, backref='members', null=True, default=None, on_delete='RESTRICT')

    created = peewee.DateTimeField(default=datetime.datetime.now)
    updated = peewee.DateTimeField(default=datetime.datetime.now)
    archived = peewee.BooleanField(default=False)

    def missing_cols(self):
        cols = []

        if not self.first_name:
            cols.append('first_name')

        if not self.last_name:
            cols.append('last_name')

        if not self.postal_code:
            cols.append('postal_code')

        if not self.phone and not self.email:
            if not self.email:
                cols.append('email')
            if not self.phone:
                cols.append('phone')

        return cols

    @classmethod
    def col_description(cls, col_name):
        return {'first_name': 'First name', 'last_name': 'Last name', 'phone': 'Phone', 'email': 'Email address', 'postal_code': 'Postal code'}[col_name]

    @hybrid_property
    def is_incomplete(self):
        """Called from code"""
        return (
            (self.first_name == '') |
            (self.last_name == '') |
            (self.postal_code == '') |
            ((self.phone == '') & (self.email == ''))
        )

    @classmethod
    def editable_cols(cls):
        return [
            'first_name', 'last_name', 'address', 'postal_code',
            'phone', 'email', 'email_consent', 'year_of_birth',
            'internal_notes']

    def name(self):
        return self.first_name + ' ' + self.last_name

    def postal_code_region(self):
        start = self.postal_code.strip().upper()[:3]
        if start in postal_codes:
            return postal_codes[start]

        return ''

    def unique_bikes(self):
        """List unique bikes from previous workorders"""

        return Workorder.select(
            peewee.fn.COUNT(Workorder.id).alias('nb'),
            Workorder.bike_description,
            Workorder.bike_serial_number,
            Workorder.created
        ) \
        .where((Workorder.client_id == self.id) & (
            (Workorder.bike_description != '') |
            (Workorder.bike_serial_number != '')
        )) \
        .group_by(Workorder.bike_description, Workorder.bike_serial_number) \
        .order_by(Workorder.created.desc())

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class WorkorderStatus(db.Model):
    id = peewee.BigIntegerField(primary_key=True, constraints=[peewee.SQL('AUTOINCREMENT')])
    name = peewee.TextField()
    color = peewee.TextField()
    display_order = peewee.IntegerField(null=True, default=None)
    archived = peewee.BooleanField(default=False)

    OPEN_ID = 1
    CLOSED_ID = 2

    def serialized(self):
        """Serialization to send via the json API"""
        serialized_item = {k: getattr(self, k) for k in ['id', 'name', 'color', 'display_order', 'archived']}

        return serialized_item

    def __str__(self):
        return self.name

class QuickButton(db.Model):
    name = peewee.TextField()
    category = peewee.TextField()
    subcategory = peewee.TextField()
    display_order = peewee.IntegerField(null=True, default=None)

    def serialized(self):
        """Serialization to send via the json API"""
        serialized_item = {k: getattr(self, k) for k in ['id', 'name', 'category', 'subcategory', 'display_order']}

        return serialized_item

    def __str__(self):
        return self.name

class Workorder(db.Model):
    """Workorders are used as a reference to compute profits, taxes,
    etc. They are both :

    - A todolist/project to be completed for workers
    - A sale
    """
    # SQLite explicit AUTOINCREMENT == don't reuse a previously delete ID
    id = peewee.BigIntegerField(primary_key=True)
    client = peewee.ForeignKeyField(Client, null=True, backref='workorders', on_delete='RESTRICT')
    type = peewee.TextField(null=True)
    bike_description = peewee.TextField(default='')
    bike_serial_number = peewee.TextField(default='')
    calendar_date = peewee.DateTimeField(default=datetime.datetime.now)
    status = peewee.ForeignKeyField(WorkorderStatus, default=WorkorderStatus.OPEN_ID, on_delete='RESTRICT')
    invoice_notes = peewee.TextField(default='')
    internal_notes = peewee.TextField(default='')
    created = peewee.DateTimeField(default=datetime.datetime.now)
    updated = peewee.DateTimeField(default=datetime.datetime.now)
    archived = peewee.BooleanField(default=False)

    # Archived values : if anything changes in the future (tax rates,
    # rebates, ...), these prices must stay
    paid_cost = peewee.DecimalField(max_digits=15, decimal_places=6, auto_round=True, null=True)
    paid_subtotal = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True, null=True)
    paid_tax1_rate = peewee.DecimalField(max_digits=15, decimal_places=8, auto_round=True, null=True)
    paid_tax2_rate = peewee.DecimalField(max_digits=15, decimal_places=8, auto_round=True, null=True)
    paid_taxes1 = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True, null=True)
    paid_taxes2 = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True, null=True)
    paid_total = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True, null=True)
    paid_date = peewee.DateTimeField(null=True, constraints=[
        # Either all of them or none of them are NULL
        peewee.SQL('''CHECK(
        ("paid_date" IS NULL) == ("paid_cost" IS NULL) AND
        ("paid_date" IS NULL) == ("paid_subtotal" IS NULL) AND
        ("paid_date" IS NULL) == ("paid_tax1_rate" IS NULL) AND
        ("paid_date" IS NULL) == ("paid_tax2_rate" IS NULL) AND
        ("paid_date" IS NULL) == ("paid_taxes1" IS NULL) AND
        ("paid_date" IS NULL) == ("paid_taxes2" IS NULL) AND
        ("paid_date" IS NULL) == ("paid_total" IS NULL)
        )''')])

    @classmethod
    def editable_cols(cls):
        return ['bike_description', 'bike_serial_number', 'calendar_date', 'status', 'invoice_notes', 'internal_notes']

    @hybrid_property
    def paid(self):
        """Called from code"""
        return self.paid_date is not None

    @paid.expression
    def paid(cls):
        """Called in SQL"""
        return cls.paid_date.is_null(False)

    def cost(self, force_calc=False):
        """Total cost of all items in the Workorder"""

        if not force_calc and self.paid:
            return self.paid_cost

        calc = Decimal('0.00')

        for item in self.items:
            calc += item.nb * item.cost

        return calc

    def subtotal(self, force_calc=False):
        """Subtotal for all items (taxable or not)"""

        if not force_calc and self.paid:
            return self.paid_subtotal

        calc = Decimal('0.00')

        for item in self.items:
            calc += item.subtotal()

        return calc

    def _calc_taxes(self, rate):
        """ABANDON ALL HOPE, YE WHO ENTERS HERE

        BE AWARE: Taxes are complex. Because of rounding, the same
        invoice can end up with two different totals post-taxes :

        sum(item_price) * taxes != sum(item_price * taxes)

              Version 1         !=       Version 2


        Eg.: item1 = 1.15$, item2 = 1.10$

        >>> from decimal import Decimal
        >>> item1 = Decimal('1.15')
        >>> item2 = Decimal('1.10')

        >>> # Subtotal is 2.25$
        >>> item1 + item2
        Decimal('2.25')

        >>> # Version 1
        >>> round((item1 + item2) * Decimal('1.15'), 2)
        Decimal('2.59')

        >>> # Version 2
        >>> round(item1 * Decimal('1.15'), 2) + round(item2 * Decimal('1.15'), 2)
        Decimal('2.58')


        The privileged method seems to be Version 1 in most places.

        For instance :
        https://www.revenuquebec.ca/fr/entreprises/taxes/tpstvh-et-tvq/perception-de-la-tps-et-de-la-tvq/calcul-des-taxes/

        > Si vous vendez plus d'un bien, vous pouvez calculer la TPS
        > et la TVQ payables sur le total des prix de tous ces biens
        > avant d'arrondir la fraction.


        This is more intuitive when looking at the Receipt Summary :

            SUBTOTAL: 2.25$
            TOTAL: 2.25$ + tx = 2.59$

        But it has the drawback of not allowing to display taxes on a
        per-line basis

                   |   Price   | Price+tx
            Item 1 |    1.10$  | 1.27$
            Item 2 |    1.15$  | 1.32$
        --------------------------------
                     Total:      2.58$ doesn't add up


        This can also lead to weird results when refunding an item
        with taxes :

                   |   Price   | Price+tx
            Item 1 |    1.10$  |  1.27$
            Item 2 |    1.15$  |  1.32$
        --------------------------------
                     Total:      2.58$ paid the first time

          Refund 1 |   -1.10$  | -1.27$
        --------------------------------

          Refund 2 |   -1.15$  | -1.32$
        --------------------------------
                     Total:      2.58$ - 1.27$ -1.32$
                              = -0.01$
                          => The client just won a free 1¢

        In the end, it seems that most people don't care that much
        about a 1¢ rounding error, although this might require a
        special handling of a full refund vs partial refund of a
        workorder... *Sigh*

        More on the subject...

        - People angry about a software that uses the Version 2
        computation :
        https://community.waveapps.com/discussion/7873/sales-tax-needs-to-be-calculated-on-subtotal

        -
        https://money.stackexchange.com/questions/23973/what-is-the-optimal-way-to-calculate-tax-apply-tax-to-each-item-then-sum-the-p

        - In Québec specifically, there are weird corner cases that
          would require an even more complex taxes system :

        https://www.lapresse.ca/affaires/finances-personnelles/201604/28/01-4975947-les-de-taxes.php
        https://ici.radio-canada.ca/tele/la-facture/site/segments/reportage/199628/amazon-livre-taxes-tps-tvq-tvh-numerique

        If the need appears, this might *maybe* be handled without
        restructuring everything with a complex system that covers all
        cases of all the countries in the world with a quick hack :

        By specifying arbitrary logic with a `def calc_tax(item):` in
        `config.py`. Such a function could bypass everything here.

        """

        assert type(rate) == Decimal

        subtotal = Decimal('0.00')

        for item in self.items.where(WorkorderItem.taxable):
            subtotal += item.subtotal()

        return round(subtotal * rate, 2)

    def taxes1(self, force_calc=False):

        if not force_calc and self.paid:
            return self.paid_taxes1

        return self._calc_taxes(Decimal(config.TAXES[0]['rate']))

    def taxes2(self, force_calc=False):

        if not force_calc and self.paid:
            return self.paid_taxes2

        return self._calc_taxes(Decimal(config.TAXES[1]['rate']))

    def taxes(self, force_calc=False):
        return self.taxes1(force_calc=force_calc) + self.taxes2(force_calc=force_calc)

    def total_discounts(self, force_calc=False):
        """Discount display on reciept"""

        if force_calc:
           raise NotImplementedError

        discount = Decimal(0)

        for item in self.items.where(WorkorderItem.price < WorkorderItem.orig_price):
            discount += (item.orig_price - item.price) * item.nb

        return round(discount, 2)

    def total(self, force_calc=False):
        return self.subtotal(force_calc=force_calc) + self.taxes(force_calc=force_calc)

    def __str__(self):
        if self.id is None:
            return 'Empty workorder'

        result = f'Workorder #{self.id}'

        if self.client:
            result += ' (for: ' + self.client.name() + ')'
        else:
            result += ' (Direct sale)'

        return result

    def set_paid(self):
        """Save archived values"""
        assert CashRegisterState.is_currently_open()

        self.status = WorkorderStatus.CLOSED_ID
        self.paid_cost = self.cost()
        self.paid_subtotal = self.subtotal()
        self.paid_tax1_rate = Decimal(config.TAXES[0]['rate'])
        self.paid_tax2_rate = Decimal(config.TAXES[1]['rate'])
        self.paid_taxes1 = self.taxes1()
        self.paid_taxes2 = self.taxes2()
        self.paid_total = self.total()
        self.paid_date = datetime.datetime.now()

    def test_invariants(self):
        """This method is used to validate that an imported database doesn't
        contain errors"""

        assert self.subtotal(force_calc=False) + self.taxes(force_calc=False) == self.total(force_calc=False), "Total error (calc=False)"
        assert self.subtotal(force_calc=True) + self.taxes(force_calc=True) == self.total(force_calc=True), "Total error (calc=True)"

        assert round(self.cost(force_calc=True), 2) == round(self.cost(force_calc=False), 2), "Cost error"
        assert round(self.subtotal(force_calc=True), 2) == self.subtotal(force_calc=False), "Subtotal error"
        assert round(self.total(force_calc=True), 2) == self.total(force_calc=False), "Total error"
        assert round(self.taxes1(force_calc=True), 2) == self.taxes1(force_calc=False), "Taxes1 error"
        assert round(self.taxes2(force_calc=True), 2) == self.taxes2(force_calc=False), "Taxes2 error"
        assert round(self.taxes(force_calc=True), 2) == self.taxes(force_calc=False), "Total taxes error"

class WorkorderItem(db.Model):
    # SQLite explicit AUTOINCREMENT == don't reuse a previously delete ID
    id = peewee.BigIntegerField(primary_key=True, constraints=[peewee.SQL('AUTOINCREMENT')])
    workorder = peewee.ForeignKeyField(Workorder, backref='items', on_delete='RESTRICT')
    inventory_item = peewee.ForeignKeyField(InventoryItem, backref='workorder_items', null=True, on_delete='RESTRICT')
    name = peewee.TextField()
    details = peewee.TextField(default='')
    nb = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True, default=1)
    price = peewee.DecimalField(max_digits=15, decimal_places=4, auto_round=True)
    # orig_price and cost are duplicated here, as the reference
    # InventoryItem might change later
    orig_price = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    cost = peewee.DecimalField(max_digits=15, decimal_places=4, auto_round=True)
    taxable = peewee.BooleanField(default=True)
    refund_item = peewee.ForeignKeyField('self', null=True, default=None, backref='refunded_by', on_delete='RESTRICT')

    @classmethod
    def editable_cols(cls):
        return ['name', 'details', 'nb', 'price', 'taxable']

    def subtotal(self):
        return self.nb * self.price

    def _calc_taxes(self, rate):
        assert type(rate) == Decimal
        return round(self.subtotal() * rate, 2)

    def taxes1(self):
        return self._calc_taxes(Decimal(config.TAXES[0]['rate'])) if self.taxable else Decimal(0)

    def taxes2(self):
        return self._calc_taxes(Decimal(config.TAXES[1]['rate'])) if self.taxable else Decimal(0)

    def serialized(self):
        """Serialization to send via the json API"""
        serialized_item = {k: getattr(self, k) for k in ['id', 'name', 'details', 'nb', 'taxable', 'inventory_item_id']}
        serialized_item['price'] = str(round(self.price, 2))
        serialized_item['orig_price'] = str(round(self.orig_price, 2))
        serialized_item['cost'] = str(round(self.cost, 2))
        serialized_item['type'] = self.inventory_item.type
        serialized_item['note'] = self.inventory_item.note

        serialized_item['refund_workorder_id'] = False
        if self.refund_item_id is not None:
            serialized_item['refund_workorder_id'] = self.refund_item.workorder.id

        serialized_item['refunded_in_workorder_id'] = False

        assert self.refunded_by.count() in [0, 1]

        if self.refunded_by.count():
            serialized_item['refunded_in_workorder_id'] = self.refunded_by.get().workorder.id

        return serialized_item

    def __str__(self):
        return self.name

class Transaction(db.Model):
    """Transactions are mostly used to keep track of cash registery and
    credit card terminal amounts. They are either related to a sale or
    to a non-sales-related amount adjustment (withdrawal/deposit).

    They are required for :

    - Unusual corner-cases (ex.: a workorder paid with a cash client
      deposit + 20$ visa)
    - Cash register balancing, to keep an history of cash register
      close/withdrawals/deposits of cash

    Total of transactions != total profits


    ASSUMPTIONS
    ===========

    Transaction.workorder_id IS **NOT** NULL
    -> This is a payment on a workorder, this is part of profits shown
       in /reports/


    TODO: REFACTOR THIS TO MAKE IT MORE EXPLICIT

    Transaction.workorder_id IS NULL
    -> Not a payment on a workorder, so either :
       a. A cash deposit/withdrawal from the cash register
       b. A deposit (down payment) from a client for an order
    -> *This is not money that you gained*
       a. -> you already owned this cash
       b. You have this money in your possession, but still owe it to
          a client

    These correspond to :
    Transaction.client_id IS NULL AND Transaction.workorder_id IS NULL
    -> Case a.
    Transaction.client_id IS NOT NULL AND Transaction.workorder_id IS NULL
    -> Case b.

    The following check :

        CHECK( NOT (client_id IS NOT NULL AND workorder_id IS NOT NULL) )

    ensures that the database cannot contain incoherent data (ie,
    referring to a client_id=X and to a workorder.client_id=Y)

    """
    TRANSACTION_TYPES = ('cash', 'interac', 'visa', 'credit account', 'check')

    # SQLite explicit AUTOINCREMENT == don't reuse a previously delete ID
    id = peewee.BigIntegerField(primary_key=True, constraints=[peewee.SQL('AUTOINCREMENT')])

    amount = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    # Model.field.choices are in the format : [(key, value), (key, value), ...]
    # To allow Flask-Admin a distinction between stored and printed value
    #
    # Although, this might change in the future...
    #    /venv/lib/python3.9/site-packages/wtforms/fields/choices.py:25:
    #      DeprecationWarning: Flags should be stored in dicts and not in tuples.
    #      The next version of WTForms will abandon support for flags in tuples.
    payment_method = peewee.TextField(choices=[(k, k) for k in TRANSACTION_TYPES],
                                      constraints=[peewee.SQL('CHECK(payment_method IN (' +
                                                              ', '.join([f"'{t}'" for t in TRANSACTION_TYPES]) +
                                                              '))')])
    workorder_id = peewee.ForeignKeyField(Workorder, null=True, backref='transactions', on_delete='RESTRICT')
    client = peewee.ForeignKeyField(Client, null=True, backref='transactions', on_delete='RESTRICT',
                                    constraints=[peewee.SQL('CHECK(NOT (client_id IS NOT NULL AND workorder_id IS NOT NULL))')])

    # Comment should be at least an empty string (but preferably not)
    # if the transaction is not related to a workorder
    comment = peewee.TextField(default=None, null=True,
                               constraints=[peewee.SQL('''
                               CHECK((comment IS NULL) == (workorder_id IS NOT NULL))
                               ''')])
    created = peewee.DateTimeField(default=datetime.datetime.now)

    def save(self, *args, **kwargs):
        assert CashRegisterState.is_currently_open(), "Cash register can only be used when open"

        return super(Transaction, self).save(*args, **kwargs)

class CashRegisterState(db.Model):
    """Store the state of the cash register at a given point in time.

    This is required for X/Z reports (to know how much of each payment
    types should

    """
    # Payment types tracked (expected_XYZ and confirmed_XYZ exist for those)
    TRACKED_PAYMENT_TYPES = ('cash', 'interac', 'visa')

    # These are the expected values, computed from the sum of
    # Transactions since the last close
    expected_cash = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    expected_visa = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    expected_interac = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)

    # These are the actual amounts, physical cash and amounts from the
    # visa machine
    confirmed_cash = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    confirmed_visa = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    confirmed_interac = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)

    cash_fund = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)
    comment = peewee.TextField(default=None, null=True)

    open = peewee.BooleanField(default=True)
    created = peewee.DateTimeField(default=datetime.datetime.now)

    @classmethod
    def is_currently_open(cls):
        state = cls.select().order_by(cls.id.desc()).get()
        return state.open

class InventoryPurchase(db.Model):
    id = peewee.BigIntegerField(primary_key=True, constraints=[peewee.SQL('AUTOINCREMENT')])
    vendor = peewee.TextField(null=True)
    reference = peewee.TextField(null=True)
    comment = peewee.TextField(null=True)
    created = peewee.DateTimeField(default=datetime.datetime.now)

    def __str__(self):
        if not self.id:
            return ''
        return f'Purchase #{self.id}, ({self.vendor} on {self.created.date()})'

class InventoryHistory(db.Model):
    """Event log to keep track of the inventory

                 ***DO NOT USE THIS MODEL DIRECTLY***
                 Use the @classmethods defined below
    """
    # TODO : Simplify : return-XYZ -> only XYZ
    EVENTS = [
        # Sold to a client
        "sale", "return-sale",
        # Purchased from manufacturer
        "purchase", "return-purchase",
        # Manually adjusted
        "manual",
    ]

    @classmethod
    def new_event(cls, event, item, delta_nb, unit_cost, workorder_id=None, purchase_id=None):

        # Reload
        item = InventoryItem.get(item.id)

        if not item.keep_track_in_inventory:
            return

        # Log the event
        history_line = InventoryHistory()
        history_line.event = event
        history_line.item = item
        history_line.delta_nb = delta_nb
        history_line.unit_cost = unit_cost
        history_line.workorder_id = workorder_id
        history_line.purchase_id = purchase_id
        history_line.save()

        """
        Average Cost Example
        ====================

        Purchases and return of stocks to the manufacturer affect the
        average cost of the item :

        - Purchase 300 @ $5 :
          - (0 * $0.00 + 300 * $5.00) / 300
        => 300 @ $5.00
        - Purchase 100 @ $6 :
          - (300 * 5 + 100 * 6) / 400
        => 400 @ $5.25
        - Return purchase 200 @ $5 : 200 @ $???
          - total_quantity = 400 - 200
          - total_value = 400 * 5.25$ - 200 * 5.00 = 1100
          - total_value / total_quantity = 5.50
        => 200 @ $5.50
        - Return purchase 100 @ 5$
          - total_quantity = 200 - 100 = 100
          - total_value = 200 * 5.50 - 100 * 5.00 = 600
          - total_value / total_quantity = 6.00
        => 100 @ $6.00

        Returns of items affect the average cost in the same way

        Sales do not affect the average cost
        """

        # Update the item's avg_cost
        if event in ['purchase', 'return-purchase', 'return-sale']:

            if event in ['purchase', 'return-sale']:
                assert delta_nb > 0
            else:
                assert delta_nb < 0

            total_quantity = item.current_inventory_count + delta_nb
            total_value = (
                item.current_inventory_count * item.avg_cost +
                delta_nb * unit_cost
            )
            new_avg_cost = total_value / total_quantity

            item.avg_cost = new_avg_cost

        elif event == 'manual':
            # Force a new avg_cost
            item.avg_cost = unit_cost

        item.current_inventory_count += delta_nb
        item.save()


    event = peewee.TextField(choices=((k, k) for k in EVENTS),
                             constraints=[peewee.SQL('CHECK(event IN (' +
                                                     ', '.join([f"'{t}'" for t in EVENTS]) +
                                                     '))')])
    item = peewee.ForeignKeyField(InventoryItem, backref='inventory_history', on_delete='RESTRICT')
    delta_nb = peewee.IntegerField(default=0)

    # This will either be an avg_cost(in the case of sales/refunds of
    # sales) or a real cost value (in the case of purchases/returns to
    # vendor)
    unit_cost = peewee.DecimalField(max_digits=15, decimal_places=2, auto_round=True)

    time = peewee.DateTimeField(default=datetime.datetime.now)
    purchase = peewee.ForeignKeyField(InventoryPurchase, null=True, backref='inventory_history', on_delete='RESTRICT')
    workorder = peewee.ForeignKeyField(Workorder, null=True, backref='inventory_history', on_delete='RESTRICT')
