from setup_app import db, app
from flask import request, redirect, url_for
from models import User, Role, UserRoles

from flask_security import Security, PeeweeUserDatastore, hash_password, current_user

"""
There are two concurrent login systems

1. A global, strong email/password login, to access the app in general

  => This is implemented by Flask-Security


2. A lightweight but frequent re-login, that prompts for an access
code

  => This ensures that being afk for a while does not allow random
     customers to toy with the POS

  => This auth is password-only and also serves to distinguish between
     employees and admins

  => This system is hand-made, and requires to be already logged in
     with in the main Flask-Security system

See confirm_identity() route in views.py
"""

user_datastore = PeeweeUserDatastore(db, User, Role, UserRoles)
security = Security(app, user_datastore)

if app.config['DEBUG']:
    @app.before_first_request
    def create_user():
        if not user_datastore.find_user(email="test@test.com"):
            user_datastore.create_user(email="test@test.com", password=hash_password("password"))

@app.before_request
def check_valid_login():
    """Basic security, must be authenticated to see anything
    Adapted from https://stackoverflow.com/a/52572337/14639652
    """
    if request.endpoint in ['static', 'security.login'] or \
       (request.endpoint in app.view_functions and
        getattr(app.view_functions[request.endpoint], 'is_public', False)) or \
        current_user.is_authenticated:
        return # Access granted
    else:
        return redirect(url_for('security.login', next=request.path))

def public_route(decorated_function):
    decorated_function.is_public = True
    return decorated_function
