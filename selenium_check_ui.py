#!/usr/bin/env python3

# Run with (unix) :
# export FLASK_ENV=development ; rm database.db* ; time flask create-db ; timeout 60 flask run & PID=$! ; ./ui_test.py ; kill $PID

from selenium import webdriver

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as Expect
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager

# ---------- Options ----------
options = webdriver.FirefoxOptions()
options.headless = True

base_url = "http://127.0.0.1:5000/"


# ---------- Wait for the server to be available ----------
import requests
from time import sleep

ok = False

for i in range(30):
    try:
        print('Trying...')
        page = requests.get(base_url)
        ok = True
        break
    except:
        sleep(1)

if not ok:
    exit(-1)


def page(path):
    return base_url + path

print('== Installing Selenium driver ==')
#driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()), options=options)

# Decommente the 3 lines under to use it with chrome
from selenium.webdriver.chrome.service import Service

from webdriver_manager.chrome import ChromeDriverManager
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))


TIMEOUT = 6

# When an element is not found, wait a bit for it to show up
driver.implicitly_wait(TIMEOUT)


# ---------- Helpers ----------
def query(selector):
    return WebDriverWait(driver, TIMEOUT).until(Expect.presence_of_element_located((By.CSS_SELECTOR, selector)))

def wait_until_invisible(selector):
    return WebDriverWait(driver, TIMEOUT).until(Expect.invisibility_of_element((By.CSS_SELECTOR, selector)))

def wait_until_visible(selector):
    return WebDriverWait(driver, TIMEOUT).until(Expect.visibility_of_element_located((By.CSS_SELECTOR, selector)))

def wait_until_text(selector, text):
    return WebDriverWait(driver, TIMEOUT).until(Expect.text_to_be_present_in_element((By.CSS_SELECTOR, selector), text))

def wait_until_value(selector, value):
    return WebDriverWait(driver, TIMEOUT).until(Expect.text_to_be_present_in_element_value((By.CSS_SELECTOR, selector), value))


test_functions = []

def add_test(f):
    test_functions.append(f)
    return f

@add_test
def login():
    driver.get(page('login'))

    email = query("[name=email]")
    password = query("[name=password]")
    submit = query("[name=submit]")

    email.send_keys("test@test.com")
    password.send_keys("password")

    submit.click()

@add_test
def set_client():
    driver.get(page('workorder/new/direct'))

    # Add a client
    query('#change-client-btn').click()
    new_client = query('#change-client')
    new_client.send_keys("Theresa Horton")

    assert wait_until_text('.tt-highlight', 'Theresa Horton')
    
    first_suggestion = query('.tt-suggestion')
    first_suggestion.click()

    assert wait_until_text('#client-editable-name', 'Theresa Horton')

@add_test
def duplicate_client():
    driver.get(page('client/new'))

    first_name = query('#first_name')
    last_name = query('#last_name')
    email = query('#email')
    phone = query('#phone')

    # Duplicate client with first and last name
    first_name.send_keys('AlicE')
    last_name.send_keys('whoopeR')

    assert wait_until_text('#similar-client-btns .name', 'Alice Whooper')

    first_name.clear()
    last_name.clear()

    # Duplicate client with first name, last name, email, phone number
    first_name.send_keys('JiMMy  ')
    last_name.send_keys('whoopeR')
    email.send_keys('      jimmy@example.com')
    phone.send_keys('(514)- 2_2.2, 3) 333')

    assert wait_until_text('#similar-client-btns .name', 'Jimmy Whooper')

    first_name.clear()
    last_name.clear()
    email.clear()
    phone.clear()

    # Duplicate client with email
    email.send_keys('theresa@example.com')

    assert wait_until_text('#similar-client-btns .name', 'Theresa Horton')

    email.clear()

    # # Duplicate client with phone number
    # phone.send_keys('514 222 3333')

    # assert wait_until_text('#similar-client-btns .name', 'Louise Howard')

@add_test
def new_direct_sale():
    driver.get(page('workorder/new/direct'))

    # Add an item
    new_item = query('#new-item')
    new_item.send_keys("Abonnement Simple")
    first_suggestion = query('.tt-suggestion')
    first_suggestion.click()

    assert wait_until_text('#subtotal', '$4.99')
    assert wait_until_text('#taxes', '$0.75')
    assert wait_until_text('.total', '$5.74')

    # Edit the item (price = 1$)
    new_item.clear()
    query('button.start-edit-item').click()
    wait_until_visible('#edit-item-modal')

    edit_price = query('#modal-edit-item-price')
    assert edit_price.is_displayed() and edit_price.is_enabled()
    edit_price.clear()
    edit_price.send_keys('1.00')

    wait_until_visible('#edit-item-modal .modal-footer button.save-btn').click()
    wait_until_invisible('#edit-item-modal')
    wait_until_invisible('.modal-backdrop.fade')


    assert wait_until_text('#subtotal', '$1.00')
    assert wait_until_text('#taxes', '$0.15')
    assert wait_until_text('.total', '$1.15')


    # Edit nb = 7
    query('button.start-edit-item').click()
    wait_until_visible('#edit-item-modal')
    edit_nb = query('#modal-edit-item-nb')
    edit_nb.clear()
    edit_nb.send_keys(7)
    query('#edit-item-modal .modal-footer button.save-btn').click()
    wait_until_invisible('.modal-backdrop.fade')

    assert wait_until_text('#subtotal', '$7.00')
    assert wait_until_text('#taxes', '$1.05')
    assert wait_until_text('.total', '$8.05')

    nb_first_item_selector = '#table-items > tbody > tr:nth-child(1) input:nth-child(1)'
    assert wait_until_value(nb_first_item_selector, '7')

    nb = query(nb_first_item_selector)
    nb.clear()
    nb.send_keys('2')

    assert wait_until_text('#subtotal', '$2.00')
    assert wait_until_text('#taxes', '$0.30')
    assert wait_until_text('.total', '$2.30')


    query('button.start-edit-item').click()
    wait_until_visible('#edit-item-modal')
    edit_taxable = query('#edit-item-modal [name="taxable"]')
    edit_taxable.click()

    query('#edit-item-modal .modal-footer button.save-btn').click()
    wait_until_invisible('.modal-backdrop.fade')

    assert wait_until_text('#subtotal', '$2.00')
    assert wait_until_text('#taxes', '$0.00')
    assert wait_until_text('.total', '$2.00')

    # Add a Second item
    new_item.send_keys("Chambre à air + Installation")
    first_suggestion = query('.tt-suggestion')
    first_suggestion.click()

    assert wait_until_text('#subtotal', '$14.00')
    assert wait_until_text('#taxes', '$1.80')
    assert wait_until_text('.total', '$15.80')

    # Play with [edit item]
    for i in range(3):
        # Edit second item
        wait_until_invisible('.modal-backdrop.fade')

        sleep(0.5)
        wait_until_visible('tbody tr:nth-child(2) button.start-edit-item').click()
        wait_until_visible('#edit-item-modal')

        assert wait_until_value('#modal-edit-item-name', 'Chambre à air + Installation')
        assert wait_until_value('#modal-edit-item-price', '12.00')
        assert wait_until_value('#modal-edit-item-nb', '1')
        assert wait_until_value('#modal-edit-item-details', '')
        assert query('#edit-item-modal [name="taxable"]').is_selected()

        sleep(0.5)
        wait_until_visible('#edit-item-modal .modal-footer button.btn-danger').click() # Cancel
        wait_until_invisible('.modal-backdrop.fade')

        sleep(0.5)
        wait_until_visible('tbody tr:nth-child(1) button.start-edit-item').click()

        assert wait_until_value('#modal-edit-item-name', 'Abonnement Simple')
        assert wait_until_value('#modal-edit-item-price', '1.00')
        assert wait_until_value('#modal-edit-item-nb', '2')
        assert wait_until_value('#modal-edit-item-details', '')
        assert not query('#edit-item-modal [name="taxable"]').is_selected()

        sleep(0.5)
        wait_until_visible('#edit-item-modal .modal-footer button.btn-danger').click() # Cancel
        wait_until_invisible('.modal-backdrop.fade')

    # Delete first element
    query('tbody tr:nth-child(1) td:last-child button.btn-danger').click()

    assert wait_until_text('#subtotal', '$12.00')
    assert wait_until_text('#taxes', '$1.80')
    assert wait_until_text('.total', '$13.80')

    # Delete remaining element
    query('tbody tr:nth-child(1) td:last-child button.btn-danger').click()

    assert wait_until_text('#subtotal', '$0.00')
    assert wait_until_text('#taxes', '$0.00')
    assert wait_until_text('.total', '$0.00')

    query('#delete').click()


@add_test
def payment():
    driver.get(page('workorder/new/direct'))

    wait_until_invisible('.modal-backdrop.fade')

    # Add an item
    new_item = query('#new-item')
    new_item.send_keys("Abonnement Simple")
    first_suggestion = query('.tt-suggestion')
    first_suggestion.click()

    assert wait_until_text('#subtotal', '$4.99')
    assert wait_until_text('#taxes', '$0.75')
    assert wait_until_text('.total', '$5.74')

    assert wait_until_visible('#pay-cash-btn')
    assert wait_until_visible('#pay-debit-btn')
    assert wait_until_visible('#pay-visa-btn')

    query('#pay-cash-btn').click()

    sleep(0.5)

    wait_until_visible('#pay-cash-received').send_keys('10')
    wait_until_value('#pay-cash-give-back', '4.26')

    query('#pay-cash-submit').click()




passed = 0

# https://stackoverflow.com/a/52742770/14639652
import traceback
for f in test_functions:
    print('===', f.__name__, '===')

    # Selenium can be kind of wobbly... Try each test multiple times
    # if needed
    for tries in range(2):
        try:
            f()
            passed += 1
            print('OK')
            break
        except Exception as ex:
            traceback.print_exception(type(ex), ex, ex.__traceback__)

print(f'Passed: {passed}/{len(test_functions)}')


driver.quit()

if passed != len(test_functions):
    exit(-1)
