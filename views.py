from setup_app import app, db, mail
from models import *

from flask import (
    render_template, request, redirect, url_for,
    session, abort, jsonify, g, send_from_directory, flash, Markup
)
from flask_mail import Message

import os
import re
import json
import datetime
from math import ceil
from functools import reduce

import peewee
from peewee import fn

from playhouse.flask_utils import get_object_or_404, PaginatedQuery

from flask_login import user_logged_in
from flask_security import current_user, logout_user
from security import public_route

from flask_security.utils import validate_redirect_url

# ---------- Basic profiling ----------
import time
import functools

def timefunc(func):
    @functools.wraps(func)
    def time_closure(*args, **kwargs):
        """time_wrapper's doc string"""
        start = time.perf_counter()
        result = func(*args, **kwargs)
        time_elapsed = time.perf_counter() - start
        print(f"Function: {func.__name__}, Time: {time_elapsed}")
        return result

    return time_closure


def or_where_conds(lst):
    return reduce(peewee.operator.or_, lst)

# ---------- Templates ----------
@app.template_filter()
def humandate(value, format="%d %b %Y"):
    """Format a date time to (Default): d Mon YYYY"""

    if type(value) is int:
        value = datetime.date.fromtimestamp(value)
    elif type(value) is str:
        value = datetime.datetime.fromisoformat(value)

    return value.strftime(format)

@app.template_filter()
def format_currency(value):
    return "${:,.2f}".format(round(value, 2))

@app.template_filter()
def format_percentage(value):
    return "{:,.2f}%".format(round(value, 2))

@app.template_test('future')
def test_future(value):
    return value > datetime.datetime.now()

@app.template_test('past')
def test_past(value):
    return value < datetime.datetime.now()

@app.template_test('close_to_today')
def test_close_to_today(value, days=7):
    delta = datetime.timedelta(days=days)
    now = datetime.datetime.now().date()
    return now - delta < value < now + delta

@app.context_processor
def inject_debug():
    return dict(debug=app.debug)

# ---------- Cash register ----------
@app.before_request
def check_cash_register_open():

    if request.endpoint in ['static', 'cash_register', 'security.login', 'security.logout'] or (
        request.endpoint in app.view_functions and
        getattr(app.view_functions[request.endpoint], 'available_if_cashregister_closed', False)) or \
        CashRegisterState.is_currently_open():
        return # Access granted

    return redirect(url_for('cash_register'))


def available_if_cashregister_closed(decorated_function):
    decorated_function.available_if_cashregister_closed = True
    return decorated_function

@app.context_processor
def inject_cash_register_open():
    return dict(cash_register_open=CashRegisterState.is_currently_open())

# ------------------------------------------------------------------------
# CONFIRM IDENTITY - Lightweight re-authentify periodically and frequently
#
# Contrary to Flask-Security's /verify/, this re-authentication uses
# the entered access code to switch the "light role" (either admin or
# employee), depending on the access code entered
#
# This access code should not be considered secure : assume that
# anyone could see you type it. The actual protection against giving
# access to anyone is Flask-Security, with a strong password.

# XXX : Flask session messes up timezone infos for date objects, using
# a float timestamp is simpler here
timestamp = lambda: datetime.datetime.utcnow().timestamp()

def remove_light_role():
    if 'light_role' in session:
        del session['light_role']

    if 'last_interaction_time' in session:
        del session['last_interaction_time']

def check_admin():

    if not current_user.is_authenticated:
        return

    if not ('light_role' in session and session['light_role'] == 'admin'):
        return redirect(url_for('confirm_identity', next=request.path))

def admin_only(decorated_function):
    """Decorator. Require the 'admin' LightRole to see the page."""
    from functools import wraps

    @wraps(decorated_function)
    def function():

        maybe_redirect = check_admin()

        if maybe_redirect:
            flash(Markup('''
            <div class="text-center">
                <p>This section is for admins only</p>
                <a href="/" class="btn btn-lg btn-success">
                    <i class="fa fa-home"></i> Take me back to the app
                </a>
            </div>
            '''.strip()),
            'danger')

            return maybe_redirect

        return decorated_function()

    return function

@app.before_request
def check_admin_flask_admin():
    # XXX : Ugly hack. Not the cleanest way to do this, but (maybe)
    # the quickest
    from admin import admin

    if request.path.startswith(admin.url):

        dummy = admin_only(lambda: None)

        return dummy()

@app.route('/confirm-identity', methods=['GET', 'POST'])
@available_if_cashregister_closed
def confirm_identity():

    if request.method == 'POST' and 'password' in request.form:

        light_role = LightRole.select().where(LightRole.password == request.form['password'])

        if light_role.exists():
            session['light_role'] = light_role.get().name
            session['last_interaction_time'] = timestamp()

            url = request.args.get('next', '/')

            if not validate_redirect_url(url):
                abort(403, "Invalid path")

            # Corner-case : when idling too long on the
            # confirm_identity form to see an admin_only route, avoid
            # being redirected back to confirm_identity again
            if next == '/confirm-identity':
                return redirect('/')

            return redirect(url)

    return render_template('confirm-identity.html')

@user_logged_in.connect_via(app)
def on_user_logged_in(sender, user):
    """Initialize "light role" on login"""
    session['last_interaction_time'] = timestamp()
    session['light_role'] = 'employee'

# Automatic logout after a certain period of inactivity
@app.before_request
def check_if_confirm_identity_required():
    """Keep track of the last interaction the current_user had with the
    app and automatically ask them to confirm their identity if they
    are inactive for too long

    """

    # The Flask-Security login is required before anything can happen
    if not current_user.is_authenticated:
        return

    # Always allow static or public routes
    if request.endpoint in ['static', 'security.login', 'security.logout'] or \
       (request.endpoint in app.view_functions and
        getattr(app.view_functions[request.endpoint], 'is_public', False)):
        return

    if 'light_role' not in session:
        if request.endpoint != 'confirm_identity':
            return redirect(url_for('confirm_identity', next=request.path))
        else:
            # Avoid infinite redirect loop when disconnecting from the
            # confirm_identity page
            return

    if session['last_interaction_time'] + app.config['ACCESS_CODE_DELAY'] <= timestamp():
        # Idle for too long, must confirm identity with the access code
        remove_light_role()

        # Avoid infinite redirect loop when disconnecting from the
        # confirm_identity page
        if request.endpoint != 'confirm_identity':
            return redirect(url_for('confirm_identity', next=request.path))

    elif request.endpoint != 'api_logged_in':
        # Update last interaction
        session['last_interaction_time'] = timestamp()


@app.route('/api/logged_in')
@available_if_cashregister_closed
@public_route
def api_logged_in():
    """This is called from JavaScript every X seconds to check if the page
    has expired

    """

    has_light_role = (
        'light_role' in session and
        'last_interaction_time' in session and
        session['last_interaction_time'] + app.config['ACCESS_CODE_DELAY'] > timestamp()
    )

    return jsonify(current_user.is_authenticated and has_light_role)

# ---------- Routes ----------
@app.route('/favicon.ico')
@public_route
@available_if_cashregister_closed
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.errorhandler(403)
def page_not_found(e):
    return render_template('403.html', e=e), 403

@app.route('/')
@available_if_cashregister_closed
def index():

    workorder_just_paid = None
    renewed_membership = None
    in_store_credit_transaction = None

    if 'paid_workorder' in request.args:

        renewed_membership = request.args.get('renewed_membership', None)

        workorder_just_paid = get_object_or_404(Workorder, (Workorder.id == request.args.get('paid_workorder')))

        if not workorder_just_paid.paid:
            abort(403)

    if 'in_store_credit_transaction' in request.args:
        transaction_id = request.args.get('in_store_credit_transaction', None)
        in_store_credit_transaction = get_object_or_404(Transaction, (Transaction.id == transaction_id))

    open_workorders = (Workorder.select()
                                .where(Workorder.paid == False)
                                .where(Workorder.type.is_null(True))
                                .where(Workorder.calendar_date > datetime.date.today() - datetime.timedelta(days=62))
                                .order_by(Workorder.calendar_date))

    today_workorders = open_workorders.where(fn.DATE(Workorder.calendar_date) == datetime.date.today())

    for w in open_workorders:
        w.group = w.calendar_date.date()

    recent_workorders = Workorder.select().where(Workorder.paid).order_by(Workorder.paid_date.desc()).limit(10)

    return render_template('index.html',
                           open_workorders=open_workorders,
                           today_workorders=today_workorders,
                           recent_workorders=recent_workorders,
                           today=datetime.date.today(),
                           in_store_credit_transaction=in_store_credit_transaction,
                           workorder_just_paid=workorder_just_paid,
                           renewed_membership=renewed_membership)

# ---------- Clients ----------
@app.route('/clients/')
@available_if_cashregister_closed
def list_clients():

    clients = Client.select().order_by(Client.created.desc())

    query = ''

    if 'query' in request.args:
        query = ' '.join(request.args['query'].split())
        terms = extract_terms(query)

        conditions = []

        for t in terms:
            if app.config['USE_SPELLFIX']:
                conditions.append(fn.editdist3(fn.lower(Client.first_name), t) <= 300)
                conditions.append(fn.editdist3(fn.lower(Client.last_name), t) <= 300)
            else:
                conditions.append(Client.first_name.contains(t))
                conditions.append(Client.last_name.contains(t))

        if conditions:
            print(conditions, 1231231231)
            clients = clients.where(or_where_conds(conditions))

    print(clients)
    clients = PaginatedQuery(clients, 25)

    return render_template('list-clients.html',
                           clients=clients.get_object_list(),
                           query=query,
                           page=clients.get_page(), total_pages=clients.get_page_count())


@app.route('/client/new/', methods=['POST', 'GET'])
@app.route('/client/new/<int:workorder_id>', methods=['POST', 'GET'])
@available_if_cashregister_closed
def new_client(workorder_id=None):
    """Create a new client, then redirect to a new or existing
    workorder"""
    client = Client()

    error = False

    if request.method == 'POST':

        if workorder_id is not None:
            workorder = get_object_or_404(Workorder, (Workorder.id == workorder_id))

        cols = Client.editable_cols()

        try:
            for col in cols:
                if col in request.form:
                    client.__setattr__(col, request.form[col].strip())
            client.save()

            if workorder_id is None:
                return redirect('/workorder/new/' + str(client.id))
            else:
                workorder.client_id = client.id
                workorder.save()
                return redirect('/workorder/' + str(workorder.id))

        except Exception as e:
            print(e)
            error = e

    return render_template('client.html',
                           new=True, error=error,
                           client=client,
                           workorder_id=workorder_id)

@app.route('/client/<int:id>')
@available_if_cashregister_closed
def get_client(id):

    client = get_object_or_404(Client, (Client.id==id))

    return render_template('client.html',
                           client=client, new=False,
                           current_workorders=client.workorders.where(~Workorder.paid).order_by(Workorder.created.desc()),
                           previous_workorders=client.workorders.where(Workorder.paid).order_by(Workorder.created.desc()))


@app.route('/make-deposit/<int:id>', methods=['POST'])
def make_deposit(id):
    form = request.form

    workorder = get_object_or_404(Workorder, (Workorder.id == id))
    if workorder.client is None:
        abort(403, "This is a direct sale, set the client if you want to register a deposit")

    client = workorder.client

    # Add two transactions, eg.: +50$ cash AND -50$ "credit account"
    # Total is still 0$ gained

    with db.database.atomic() as tr:
        # The transaction made by the client
        client_transaction = Transaction.create(
            amount=round(Decimal(form['amount']), 2),
            payment_method=form['deposit_type'],
            workorder_id=None,
            client=client.id,
            comment="Deposit",
        )

        balanced_transaction = Transaction.create(
            amount=-round(Decimal(form['amount']), 2),
            payment_method='credit account',
            workorder_id=None,
            client=client.id,
            comment="Deposit",
        )

        client.in_store_credit += Decimal(form['amount'])
        client.save()

    return redirect(url_for('index', in_store_credit_transaction=client_transaction.id))


# ---------- Workorders/invoices ----------
@app.route('/workorders/')
@app.route('/workorders/<int:page>')
@app.route('/workorders/type/<string:workorder_type>/')
@app.route('/workorders/type/<string:workorder_type>/<int:page>')
@available_if_cashregister_closed
def list_workorders(workorder_type=None, page=1):
    date_start = datetime.date.today() - datetime.timedelta(days=365)
    date_end = datetime.date.today()
    status_conditions = True
    client_conditions = True
    request_status = []
    advanced_options = { 'archived' : False, 'paid': True, 'non-paid': True}

    if workorder_type and workorder_type not in app.config['EXTRA_WORKORDER_TYPES']:
        abort(403, f"No such workorder type. Maybe you should add `{workorder_type}` in your config.py?")
    elif workorder_type:
        advanced_options['paid'] = not advanced_options['paid']

    modified_options = advanced_options.copy()

    query = ''
    page_query = ''
    if 'query' in request.args:
        query = ' '.join(request.args['query'].split())
        terms = extract_terms(query)

        query_conditions = []

        full_name_match = Client.select().where((Client.first_name + ' ' + Client.last_name == query))
        if full_name_match:
            for client in full_name_match:
                query_conditions.append(Client.id == client.id)
        else:
            for t in terms:
                if app.config['USE_SPELLFIX']:
                    query_conditions.append(fn.editdist3(fn.lower(Client.first_name), t) <= 300)
                    query_conditions.append(fn.editdist3(fn.lower(Client.last_name), t) <= 300)
                else:
                    query_conditions.append(Client.first_name.contains(t))
                    query_conditions.append(Client.last_name.contains(t))

        if query_conditions:
            client_conditions = or_where_conds(query_conditions)

    if 'date_start' in request.args:
        date_start = datetime.date.fromisoformat(request.args['date_start'])

    if 'date_end' in request.args:
        date_end = datetime.date.fromisoformat(request.args['date_end'])

    if 'status' in request.args:
        request_status = request.args.getlist('status')

        status_conditions = []
        for status_id in request_status:
            status_conditions.append(Workorder.status == status_id)

        status_conditions = or_where_conds(status_conditions)

    if 'advanced' in request.args:
        request_advanced = request.args.getlist('advanced')

        for option in request_advanced:
            if option in advanced_options.keys():
                modified_options[option] = not advanced_options[option]

    paid_conds = True
    if not modified_options['paid'] or not modified_options['non-paid']:
        paid_conds = (Workorder.paid == False)
        if not modified_options['paid'] and not modified_options['non-paid']:
            paid_conds = False
        elif not modified_options['non-paid']:
            paid_conds = (Workorder.paid == True)

    archived_conds = (Workorder.archived == False)
    if modified_options['archived']:
        archived_conds = True

    advanced_conditions = (archived_conds) & (paid_conds)

    per_page = 25

    workorders =  (Workorder.select()
                            .where((fn.DATE(Workorder.created).between(date_start, date_end))
                                & (status_conditions) & (advanced_conditions))
                            .join(Client, peewee.JOIN.LEFT_OUTER)
                            .where(client_conditions)
                            .order_by(Workorder.created.desc()))

    if workorder_type:
        workorders = workorders.where(Workorder.type == workorder_type)

    total_pages = ceil(workorders.count() / per_page)
    workorders = workorders.paginate(page, per_page)

    workorderstatuses = (WorkorderStatus.select()
                         .where(WorkorderStatus.archived == False)
                         .order_by(WorkorderStatus.display_order))

    for status in workorderstatuses:
        if str(status.id) in request_status:
            status.is_active = True

    page_query = {}
    for req in request.args:
        page_query[req] = request.args.getlist(req)

    if workorder_type:
        page_query['workorder_type'] = workorder_type

    if page > total_pages:
        return redirect(url_for('list_workorders',
                                page=total_pages,
                                **page_query))

    return render_template('list-workorders.html',
                           workorders=workorders,
                           workorder_type=workorder_type,
                           workorder_type_details=workorder_type and app.config['EXTRA_WORKORDER_TYPES'][workorder_type],
                           advanced_options=advanced_options,
                           modified_options=modified_options,
                           page=page, total_pages=total_pages,
                           date_start=date_start, date_end=date_end,
                           query=query, page_query=page_query,
                           statuses=workorderstatuses)

@app.route('/workorder/delete/<int:id>')
@available_if_cashregister_closed
def delete_workorder(id):

    try:
        # TODO : pragma checks are not always working
        assert list(db.database.execute_sql('pragma foreign_keys'))[0][0] == 1

        WorkorderItem.delete().where(WorkorderItem.workorder_id==id).execute()
        Workorder.delete().where(Workorder.id==id).execute()

        flash('Workorder deleted', 'info')
    except Exception as e:
        print(e)
        flash('This workorder cannot be deleted', 'danger')

    return redirect("/")

@app.route('/workorder/new/direct')
@app.route('/workorder/new/<int:client_id>')
@available_if_cashregister_closed
def new_workorder(client_id=None):

    # TODO : Check for empty workorders?  Empty workorders can be
    # created by mistake, we should have a way to delete those to
    # avoid polluting the database

    if 'type' in request.args:
        if request.args['type'] not in app.config['EXTRA_WORKORDER_TYPES']:
            abort(403, f"No such workorder type. Maybe you should add `{request.args['type']}` in your config.py?")
        workorder = Workorder.create(client_id=client_id, type=request.args['type'])
    else:
        workorder = Workorder.create(client_id=client_id)

    return redirect(url_for('get_workorder', id=workorder.id))

@app.route('/workorder/refund/<int:id>')
def refund_workorder(id):

    workorder = get_object_or_404(Workorder, (Workorder.id==id))

    # Checks if all items are either refunded or refunds of other items
    total_workorder_items = workorder.items.count()
    refund_items = workorder.items.where(WorkorderItem.refund_item_id.is_null(False)).count()
    refunded_items = workorder.items.where(WorkorderItem.id << WorkorderItem.select(WorkorderItem.refund_item_id).where(WorkorderItem.refund_item_id.is_null(False))).count()
    refunded = (refund_items + refunded_items) == total_workorder_items

    if refunded or not workorder.paid:
        return redirect('/workorder/' + str(workorder.id))

    items = []

    for i in workorder.items.where(WorkorderItem.refund_item_id.is_null(True)):
        item = i.serialized()
        # if not item['refunded_in_workorder_id']:
        items.append(item)

    return render_template('refund.html',
                           workorder=workorder,
                           items=items)

@app.route('/workorder/<int:id>')
@available_if_cashregister_closed
def get_workorder(id):

    workorder = get_object_or_404(Workorder, (Workorder.id==id))
    client = workorder.client

    class_client = Client
    # Propose to use most recent bike if no bike is set yet
    last_bike = {}
    if not workorder.bike_description and not workorder.bike_serial_number and workorder.client:
        last_workorder = (
            workorder
            .client
            .workorders
            .where(
                (Workorder.id != id) &
                (
                    (Workorder.bike_description != '') |
                    (Workorder.bike_serial_number != '')
                ))
            .order_by(Workorder.created.desc())
            .limit(1)
        )

        if last_workorder.count() > 0:
            last_workorder = last_workorder.get()
            last_bike['bike_description'] = last_workorder.bike_description
            last_bike['bike_serial_number'] = last_workorder.bike_serial_number

    items = []

    for i in workorder.items:
        items.append(i.serialized())


    # Check if all items are either refunded or refunds of other items
    total_workorder_items = workorder.items.count()
    refund_items = workorder.items.where(WorkorderItem.refund_item_id.is_null(False)).count()
    # FIXME : this query seems to be heavier than necessary
    # The subquery here might fetch a lot of IDs
    #
    # Inspect it with EXPLAIN QUERY PLAN and optimize if required
    refunded_items = (
        workorder
        .items
        .where(
            WorkorderItem.id.in_(
                WorkorderItem.select(WorkorderItem.refund_item_id)
                .where(WorkorderItem.refund_item_id.is_null(False)))
        ).count()
    )
    refunded = (refund_items + refunded_items) == total_workorder_items

    quick_items = QuickButton.select(QuickButton.category, QuickButton.subcategory, QuickButton.name).order_by(QuickButton.display_order).dicts()

    statuses = WorkorderStatus.select().where(WorkorderStatus.archived==False).order_by(WorkorderStatus.display_order)

    # FIXME : membership logic will probably not be usefull to
    # everyone, there should be a way to define instance-specific
    # hooks instead of hardcoding that kind of logic here
    membership_item_ids = [i.id for i in InventoryItem.select(InventoryItem.id).where(InventoryItem.special_meaning=='membership')]

    # XXX : join?
    propose_membership = (
        workorder.client is not None and
        (
            workorder.client.membership_paid_until is None or
            test_past(workorder.client.membership_paid_until)
        ) 
        # and not workorder.items.where(WorkorderItem.inventory_item_id.in_(membership_item_ids)).exists()
    )

    # Hide bike infos for direct sales (client_id IS NULL), unless the info has been
    # filled
    # TODO : Should we always show it for special workorders?
    show_bike_infos = (
        workorder.type is not None or
        workorder.client is not None or
        workorder.bike_description or
        workorder.bike_serial_number or
        workorder.invoice_notes or workorder.internal_notes
    )

    return render_template('workorder.html',
                           workorder=workorder,
                           show_bike_infos=show_bike_infos,
                           last_bike=last_bike,
                           items=items,
                           statuses=statuses,
                           refunded=refunded,
                           refund_items=refund_items,
                           quick_items=quick_items,
                           membership_item_ids=membership_item_ids,
                           propose_membership=propose_membership,
                           client=client,
                           Client=Client)

@app.route('/workorder/receipt/<int:workorder_id>')
@available_if_cashregister_closed
def workorder_receipt(workorder_id):

    workorder = get_object_or_404(Workorder, (Workorder.id==workorder_id))

    return render_template('receipt.html', workorder=workorder)

@app.route('/deposit/receipt/<int:transaction_id>')
@available_if_cashregister_closed
def deposit_receipt(transaction_id):

    transaction = get_object_or_404(Transaction, (Transaction.id==transaction_id))

    return render_template('deposit-receipt.html', transaction=transaction)

@app.route('/workorder/email-receipt/<int:workorder_id>', methods=['POST'])
def workorder_email_receipt(workorder_id):

    workorder = get_object_or_404(Workorder, (Workorder.id==workorder_id))

    if not workorder.paid:
        abort(403, "Email receipts only works on paid workorders")

    if 'email' not in request.form:
        abort(403, 'No email specified')

    recipient = request.form['email']

    if 'save-email' in request.form:

        client = workorder.client

        if client is None:
            abort(403, "Can't save the address if there is no client")

        client.email = recipient
        client.save()

    msg = Message(f"Receipt from {app.config['COMPANY_NAME']}", recipients=[recipient])

    msg.html = render_template('email-receipt.html',
                               workorder=workorder,
                               client=workorder.client)

    mail.send(msg)

    flash(Markup('Receipt send to <b>%s</b>') % recipient, 'success')

    return redirect("/")

@app.route('/deposit/email-receipt/<int:transaction_id>', methods=['POST'])
def deposit_email_receipt(transaction_id):

    transaction = get_object_or_404(Transaction, (Transaction.id==transaction_id))

    if 'email' not in request.form:
        abort(403, 'No email specified')

    recipient = request.form['email']

    if 'save-email' in request.form:
        client = transaction.client

        if client is None:
            abort(403, "Can't save the address if there is no client")

        client.email = recipient
        client.save()

    msg = Message(f"Receipt from {app.config['COMPANY_NAME']}", recipients=[recipient])

    msg.html = render_template('deposit-email-receipt.html',
                               transaction=transaction)

    mail.send(msg)

    flash(Markup('Receipt send to <b>%s</b>') % recipient, 'success')

    return redirect("/")

@app.route('/workorder/set_client/<int:workorder_id>')
@app.route('/workorder/set_client/<int:workorder_id>/<int:client_id>')
@available_if_cashregister_closed
def set_workorder_client(workorder_id, client_id=None):

    workorder = get_object_or_404(Workorder, (Workorder.id==workorder_id))

    if client_id is not None:
        # Assert the client exists
        get_object_or_404(Client, (Client.id==client_id))

    workorder.client_id = client_id
    workorder.save()

    return redirect('/workorder/' + str(workorder.id))

@app.route('/workorder/pay/<int:workorder_id>', methods=['POST'])
def pay_workorder(workorder_id):
    form = request.form

    # Partial payments are not authorized, to avoid corner cases
    # where someone would have partially paid more than the final
    # total price
    #
    # Deposits are handled as a separate process that is not attached
    # to a specific workorder, only to a client account

    workorder = get_object_or_404(Workorder, (Workorder.id==workorder_id))

    # Don't re-pay the same workorder twice and don't allow empty
    # workorders to be paid
    if workorder.paid:
        abort(403, "This workorder has already been paid.")

    if not workorder.items.count():
        abort(403, "This workorder is empty.")

    # ---------- Handle missing infos ----------
    if workorder.client:
        client = workorder.client

        if "last_name" in request.form:
            client.last_name = request.form["last_name"]
        if "first_name" in request.form:
            client.first_name = request.form["first_name"]
        if "postal_code" in request.form:
            client.postal_code = request.form["postal_code"]
        if "email" in request.form:
            client.email = request.form["email"]
        if "phone" in request.form:
            client.phone = request.form["phone"]

        client.save()

    # ---------- Create the transaction(s) ----------
    # Note : 0$ workorders are allowed (as long as there is at least
    # one item), but no transaction should be recorded
    #
    # A "paid" 0$ workorder is a closed free workorder
    if workorder.total() != 0:

        if 'refund_as_in_store_credit' in form:

            if workorder.client is None:
                abort(403, "Cannot refund direct sales as store credit, you should set a client if you want to do that")

            transaction = Transaction.create(
                amount=workorder.total(),
                payment_method='credit account',
                workorder_id=workorder_id,
            )

            workorder.client.in_store_credit -= workorder.total()
            workorder.client.save()

        elif 'payment_method' in form: # Single transaction
            method = form['payment_method']

            if method not in Transaction.TRANSACTION_TYPES:
                abort(403, "Not a valid payment method")

            transaction = Transaction.create(
                amount=workorder.total(),
                payment_method=method,
                workorder_id=workorder_id,
            )

        else: # Multiple transactions

            total_amount = 0
            payment_methods = {}
            for method in form:
                try:
                    amount = Decimal(form[method])
                except:
                    amount = None
                if amount and method in Transaction.TRANSACTION_TYPES:
                    payment_methods[method] = amount

                    # Refunds should require users to enter negative amounts
                    if workorder.total() < 0:
                        payment_methods[method] *= -1

                    total_amount += payment_methods[method]

            if total_amount != workorder.total():
                abort(403, "Payment does not balance")

            if 'credit account' in payment_methods:
                if payment_methods['credit account'] > workorder.client.in_store_credit:
                    abort(403, "Credit submitted excedes client credit")

            if 'credit account' in payment_methods and workorder.total() < 0:
                raise NotImplementedError("Partial refund to credit account is not supported yet")

            for method, amount in payment_methods.items():
                assert amount != 0

                if method == 'credit account':
                    workorder.client.in_store_credit -= amount
                    workorder.client.save()

                Transaction.create(
                    amount=amount,
                    payment_method=method,
                    workorder_id=workorder_id,
                )

    workorder.set_paid()
    workorder.save()

    # ---------- Adjust inventory ----------
    for workorderitem in workorder.items.join(InventoryItem).where(InventoryItem.keep_track_in_inventory):
        if workorderitem.nb == 0:
            continue

        event = 'sale' if workorderitem.nb > 0 else 'return-sale'

        InventoryHistory.new_event(event, InventoryItem.get(workorderitem.inventory_item_id), -workorderitem.nb, workorderitem.cost)

    # ---------- Membership ----------
    renewed_membership = None

    # If there is a membership item, set membership expiration date to
    # +1 year. TODO : Find a way to use external hooks instead of
    # hardcoding this
    if workorder.client:
        client = workorder.client
        for item in workorder.items:
            # Avoid granting membership during refunds (nb < 0)
            # TODO : Refunding a membership? How would that work?

            if item.inventory_item and item.nb > 0 and item.inventory_item.special_meaning == 'membership':
                client.membership_paid_until = datetime.datetime.now() + datetime.timedelta(days=365)
                client.membership_item_id = item.inventory_item_id
                renewed_membership = client.membership_paid_until.date()
                client.save()
                break

    return redirect(url_for('index', paid_workorder=workorder_id, renewed_membership=renewed_membership))

# ---------- Admin ----------
def compute_workorder_stats_slow(date_start, date_end):

    paid_workorders = (Workorder.select()
                         .where(
                             Workorder.paid &
                             fn.DATE(Workorder.paid_date).between(date_start, date_end)
                         ))

    workorder_stats = {
        'cost': Decimal(0),
        'subtotal': Decimal(0),
        'total': Decimal(0),
        'taxes1': Decimal(0),
        'taxes2': Decimal(0),
        'taxes': Decimal(0),
        'total_discounts': Decimal(0),
    }

    for workorder in paid_workorders:
        workorder_stats['cost'] += workorder.cost()
        workorder_stats['subtotal'] += workorder.subtotal()
        workorder_stats['total'] += workorder.total()
        workorder_stats['taxes1'] += workorder.taxes1()
        workorder_stats['taxes2'] += workorder.taxes2()
        workorder_stats['taxes'] += workorder.taxes()
        workorder_stats['total_discounts'] += workorder.total_discounts()

    return workorder_stats

def compute_workorder_stats(date_start, date_end):
    """Compute statistics about payments directly in SQL

    This is tricky to implement correctly, as there is no Decimal()
    datatype in SQLite. Instead, all computations are done in the
    Integer format, to avoid floating point rounding errors of the
    kind : 0.1 + 0.2 != 0.3

    The pattern :

        CAST(ROUND(paid_subtotal* 100) AS INTEGER)

    Is used to compute only with integers at a precision of two
    decimals. Once the result is fetched, it can be divided back by
    100 in python using the Decimal() datatype

    """
    paid_workorders = (
        Workorder
        .select(
            peewee.SQL('COALESCE(SUM(CAST(ROUND(paid_cost    * 100) AS INTEGER)), 0) as paid_cost'),
            peewee.SQL('COALESCE(SUM(CAST(ROUND(paid_subtotal* 100)   AS INTEGER)), 0) as paid_subtotal'),
            peewee.SQL('COALESCE(SUM(CAST(ROUND(paid_total   * 100)   AS INTEGER)), 0) as paid_total'),
            peewee.SQL('COALESCE(SUM(CAST(ROUND(paid_taxes1  * 100)   AS INTEGER)), 0) as paid_taxes1'),
            peewee.SQL('COALESCE(SUM(CAST(ROUND(paid_taxes2  * 100)   AS INTEGER)), 0) as paid_taxes2'),
        )
        .where((Workorder.paid) & (fn.DATE(Workorder.paid_date).between(date_start, date_end)))
    ).get()

    # To achieve some specific discounts, some items might need a
    # price with more than two digits (eg.: an odd discounted price
    # for an even number of the same item, such as $1.01 paid for 2
    # reflectors
    workorderitem_table = WorkorderItem.alias('workorderitem')
    total_discounts = (
        Workorder
        .select(
            peewee.SQL('''COALESCE(SUM(
                         (CAST(ROUND(workorderitem.orig_price * 1000) as INTEGER)
                           - CAST(ROUND(workorderitem.price * 1000) as INTEGER)
                          ) * CAST(ROUND(workorderitem.nb * 1000) as INTEGER)
                         ), 0) as discount''')
        )
        .join(workorderitem_table)
        .where(
            (Workorder.paid) &
            (fn.DATE(Workorder.paid_date).between(date_start, date_end)) &
            (workorderitem_table.price < workorderitem_table.orig_price)
        )
    ).get()

    workorder_stats = {
        'cost': Decimal(paid_workorders.paid_cost) / 100,
        'subtotal': Decimal(paid_workorders.paid_subtotal) / 100,
        'total': Decimal(paid_workorders.paid_total) / 100,
        'taxes1': Decimal(paid_workorders.paid_taxes1) / 100,
        'taxes2': Decimal(paid_workorders.paid_taxes2) / 100,
        'taxes': (Decimal(paid_workorders.paid_taxes1) + Decimal(paid_workorders.paid_taxes2)) / 100,

        # XXX : Multiplications of integers representing decimals have
        # to be corrected twice
        'total_discounts': Decimal(total_discounts.discount) / 1_000_000,
    }

    return workorder_stats


@app.route('/reports/')
@timefunc
@available_if_cashregister_closed
@admin_only
def reports():

    date_start = datetime.date.today() - datetime.timedelta(days=365)
    date_end = datetime.date.today()

    if 'date_start' in request.args:
        date_start = datetime.date.fromisoformat(request.args['date_start'])

    if 'date_end' in request.args:
        date_end = datetime.date.fromisoformat(request.args['date_end'])


    postal_code_query = (
        Client
        .select(
            fn.UPPER(fn.SUBSTR(Client.postal_code, 1, 3)).alias('pc'),
            fn.COUNT(peewee.SQL('*')).alias('nb')
        )
        .where(Client.postal_code != '')
        .where(Client.created.between(date_start, date_end))
        .group_by(peewee.SQL('pc'))
        .order_by(peewee.SQL('nb').desc())
    )

    # TOP 5
    postal_codes_stats = list(postal_code_query.limit(5).dicts())
    postal_codes_keys = [line['pc'] for line in postal_codes_stats]
    postal_codes_vals = [line['nb'] for line in postal_codes_stats]

    # Others (known)
    postal_codes_others = (
        Client
        .select()
        .where(
            (fn.UPPER(fn.SUBSTR(Client.postal_code, 1, 3)).not_in(postal_codes_keys)) &
            (Client.postal_code != '') &
            (Client.created.between(date_start, date_end))
        )
        .count()
    )

    # Unknown
    postal_codes_unknown = (
        Client
        .select()
        .where(
            (Client.created.between(date_start, date_end)) &
            (Client.postal_code == '')
        )
        .count()
    )

    # Assert that all clients are accounted for
    all_clients_count = Client.select().where(
        Client.created.between(date_start, date_end)
    ).count()

    assert all_clients_count == sum(postal_codes_vals + [postal_codes_others, postal_codes_unknown])

    for i in range(len(postal_codes_keys)):
        key = postal_codes_keys[i]

        if key in postal_codes:
            postal_codes_keys[i] = key + ' (' + postal_codes[key] + ')'

    items_sold = (
        InventoryItem
        .select(InventoryItem.name, fn.sum(WorkorderItem.nb).alias("total_sold"))
        .join(WorkorderItem)
        .join(Workorder)
        .where(
            InventoryItem.keep_track_in_inventory &
            Workorder.paid &
            fn.DATE(Workorder.paid_date).between(date_start, date_end)
        )
        .group_by(InventoryItem.id)
        .order_by(fn.sum(WorkorderItem.nb).desc())
    )

    profit_margin = 0

    workorder_stats = compute_workorder_stats(date_start, date_end)

    profits = workorder_stats['subtotal'] - workorder_stats['cost']

    if profits != 0 and workorder_stats['subtotal'] != 0:
        profit_margin = (profits / workorder_stats['subtotal']) * 100

    return render_template('reports.html',
                           show_postal_codes=bool(len(postal_codes_keys)),
                           postal_codes_keys=postal_codes_keys + ['Others', 'Unknown'],
                           postal_codes_vals=postal_codes_vals + [postal_codes_others, postal_codes_unknown],
                           date_start=date_start,
                           date_end=date_end,
                           workorder_stats=workorder_stats,
                           items_sold=items_sold,
                           profits=profits,
                           profit_margin=profit_margin)

@app.route('/inventory/', methods=['POST', 'GET'])
@admin_only
def inventory():
    if request.method == 'POST':
        purchase = InventoryPurchase()
        purchase.vendor = request.form['vendor']
        purchase.reference = request.form['reference-number']
        purchase.comment = request.form['comments']
        purchase.created = datetime.datetime.now()
        purchase.save()
        purchase_id = InventoryPurchase.select().order_by(InventoryPurchase.id.desc()).get()

        for id, cost, quantity in zip(request.form.getlist('id'),
                                      request.form.getlist('current-cost'),
                                      request.form.getlist('quantity')):
            if quantity:
                if int(quantity) > 0:
                    item = InventoryItem.get(int(id))
                    InventoryHistory.new_event('purchase', item, int(quantity), int(cost), purchase_id=purchase_id)

    inventory_items = InventoryItem.select().where(InventoryItem.keep_track_in_inventory)

    return render_template('inventory.html',
                            inventory_items=inventory_items)

@app.route('/api/inventory-history/<int:id>')
def inventory_history(id):
    query = InventoryHistory.select().where(InventoryHistory.item_id == id)

    query = query.dicts()
    results = []
    for item in query:
        results.append(item)

    return jsonify({
        'results': results,
    })

def compute_cash_register_stats():
    last_interaction = CashRegisterState.select().order_by(CashRegisterState.created.desc()).get()

    cash_register_stats = {
        'cash': Decimal(0),
        'interac': Decimal(0),
        'visa': Decimal(0),
        'credit account': Decimal(0),
    }

    for method in cash_register_stats.keys():
        transactions = Transaction.select().where(
            (Transaction.payment_method == method) &
            (Transaction.created > last_interaction.created)
        )

        for t in transactions:
            cash_register_stats[method] += t.amount

    # Compute added/spent in-store credit added since last interaction
    cash_register_stats['new_in_store_credit'] = -Decimal(Transaction.select(
        peewee.SQL('COALESCE(SUM(CAST(ROUND(amount * 100) AS INTEGER)), 0) as amount'),
    ).where(
        (Transaction.payment_method == 'credit account') &
        (Transaction.amount < 0) &
        (Transaction.created > last_interaction.created)
    ).scalar()) / 100

    cash_register_stats['used_in_store_credit'] = Decimal(Transaction.select(
        peewee.SQL('COALESCE(SUM(CAST(ROUND(amount * 100) AS INTEGER)), 0) as amount'),
    ).where(
        (Transaction.payment_method == 'credit account') &
        (Transaction.amount > 0) &
        (Transaction.created > last_interaction.created)
    ).scalar()) / 100

    # Include the cash fund in Total Cash
    last_cash_fund = Decimal(0) if last_interaction.cash_fund is None else last_interaction.cash_fund
    cash_register_stats['total_cash'] = cash_register_stats['cash'] + last_cash_fund

    cash_register_stats['last_cash_fund'] = last_cash_fund

    # Compute all deposits
    deposits = Decimal(
        Transaction
        .select(peewee.SQL('COALESCE(SUM(CAST(ROUND(amount  * 100) AS INTEGER)), 0)'))
        .where((Transaction.workorder_id == None) &
                (Transaction.client == None) &
               (Transaction.created > (last_interaction.created))
        ).scalar()) / 100

    cash_register_stats['deposits'] = deposits

    # Total amount of collected money (through deposits/in-store credits or payments)
    # This is not shown, but it is used in tests
    cash_register_stats['total'] = (
        cash_register_stats['cash'] +
        cash_register_stats['interac'] +
        cash_register_stats['visa']
    )

    return cash_register_stats

@app.route('/cash-register/', methods=['POST', 'GET'])
@admin_only
def cash_register():
    last_interaction = CashRegisterState.select().order_by(CashRegisterState.created.desc()).get()
    cash_register_stats = compute_cash_register_stats()

    default_cash_fund = Decimal('0')

    if request.method == 'POST':
        form = request.form
        register = CashRegisterState()

        # Deposit/withdrawal
        if 'deposit' in form:

            if not CashRegisterState.is_currently_open():
                flash(f"Must open cash register before making a transaction", 'warning')
                return redirect(url_for('cash_register'))

            amount = round(Decimal(form['amount']), 2)

            if form['deposit'] == 'withdrawal':
                amount = -amount

            Transaction.create(
                amount=amount,
                payment_method='cash',
                comment=form['comment'] # XXX
            )

            flash(f"Cash {form['deposit']} of {format_currency(abs(amount))} recorded", 'success')

        # Open/Close cash register
        else:
            if CashRegisterState.is_currently_open():
                register.open = False
                flash("Cash register closed successfully", 'info')
            else:
                register.open = True
                flash("Cash register opened successfully", 'info')

            for method in CashRegisterState.TRACKED_PAYMENT_TYPES:
                setattr(register, f'expected_{method}', cash_register_stats[method])
                setattr(register, f'confirmed_{method}', form[method])

            register.cash_fund = form['cash_fund']
            register.comment = form['comment'].strip() or None
            register.save()

        return redirect('/cash-register/')

    # --- Infos about last close ---
    last_close_date = "Never"

    last_close = (
        CashRegisterState.select()
        .where(~CashRegisterState.open)
        .order_by(CashRegisterState.created.desc())
    )

    if last_close.exists():
        last_close = last_close.get()
        default_cash_fund = Decimal(last_close.cash_fund)
        last_close_date = humandate(last_close.created, "%d %b %Y %H:%M")

    return render_template('cash-register.html',
                           last_close=last_close,
                           date=last_close_date,
                           cash_register_stats=cash_register_stats,
                           payment_methods = CashRegisterState.TRACKED_PAYMENT_TYPES)

# ---------- API ----------
@app.route('/api/edit/<model>/<int:id>', methods=['POST'])
@available_if_cashregister_closed
def edit_model(model, id):
    """Edit one column from the model"""

    authorized_models = {'client': Client, 'workorderitem': WorkorderItem, 'workorder': Workorder}

    klass = authorized_models[model]

    form = request.json
    column = form['column']
    value = form['value']

    if klass == WorkorderItem:
        workorderitem = klass.get(id)
        if workorderitem.workorder.paid:
            abort(403)

    if klass == Workorder and column == 'calendar_date':
        try:
            value = datetime.datetime.fromisoformat(value)
        except:
            print('ERROR', model, id, column, value)
            abort(403)


    allowed_columns = klass.editable_cols()

    if column not in allowed_columns:
        abort(403)

    klass.update({column: value}).where(klass.id == id).execute()

    return jsonify(True)

@app.route('/api/replace/workorderitem/<int:id>', methods=['POST'])
@available_if_cashregister_closed
def replace_model(id):
    """Replace a full row, only for editable columns"""

    workorderitem = get_object_or_404(WorkorderItem, (WorkorderItem.id==id))

    if workorderitem.workorder.paid:
        abort(403)

    form = request.json

    for col in WorkorderItem.editable_cols():
        if col in form:
            workorderitem.__setattr__(col, form[col])

    workorderitem.save()

    return jsonify(WorkorderItem.get(id).serialized())


@app.route('/api/total/<int:workorder_id>')
@available_if_cashregister_closed
def get_workorder_total_infos(workorder_id):

    workorder = get_object_or_404(Workorder, (Workorder.id==workorder_id))
    total_workorder_items = workorder.items.count()

    return jsonify({
        'subtotal': round(workorder.subtotal(), 2),
        'discount': round(workorder.total_discounts(), 2),
        'taxes1': round(workorder.taxes1(), 2),
        'taxes2': round(workorder.taxes2(), 2),
        'taxes': round(workorder.taxes(), 2),
        'total': round(workorder.total(), 2),
        'nb_items': total_workorder_items,
        'workorder_paid': workorder.paid,
    })

@app.route('/api/add-item/<int:workorder_id>/<int:inventory_item_id>')
@available_if_cashregister_closed
def add_item(workorder_id, inventory_item_id):


    workorder = get_object_or_404(Workorder, (Workorder.id==workorder_id))
    inventory_item = get_object_or_404(InventoryItem, (InventoryItem.id==inventory_item_id))

    # We shouldn't modify an invoice once it's paid
    if workorder.paid:
        abort(403)

    item = WorkorderItem.create(
        name=inventory_item.name,
        price=inventory_item.price,
        cost=inventory_item.avg_cost,
        orig_price=inventory_item.price,
        taxable=inventory_item.taxable,
        workorder_id=workorder_id, inventory_item_id=inventory_item_id
    )

    return jsonify(item.serialized())

@app.route('/api/delete-workorderitem/<int:workorderitem_id>')
@available_if_cashregister_closed
def remove_item(workorderitem_id):

    workorderitem = get_object_or_404(WorkorderItem, (WorkorderItem.id==workorderitem_id))

    if workorderitem.workorder.paid:
        abort(403)

    WorkorderItem.delete().where(WorkorderItem.id==workorderitem_id).execute()

    return jsonify(True)


def extract_terms(s):
    terms = [term.strip() for term in s.split()]
    terms = set([t.lower() for t in terms if t])
    return set(terms)

# https://stackoverflow.com/a/17741165/14639652
import difflib

def fuzzy_matches(large_string, query_string, threshold):
    words = large_string.split()
    for word in words:
        s = difflib.SequenceMatcher(None, word, query_string)
        match = ''.join(word[i:i+n] for i, j, n in s.get_matching_blocks() if n)

        score = len(match) / float(len(query_string))

        if score >= threshold:
            yield match, len(match)

def does_match(needle, haystack):
    if len(needle) < 2:
        return 0
    if len(needle) <= 3:
        return (needle.lower() in haystack.lower()) * 0.5
    else:
        all_matches = list(fuzzy_matches(haystack.lower(), needle.lower(), 0.8))
        nb = len(all_matches)

        if not nb:
            return 0

        return sum([x[1] for x in all_matches])


@app.route('/api/search/inventory_items/', methods=['GET'])
@timefunc
@available_if_cashregister_closed
def api_get_inventory_items():
    # Old python-based version in commit 4619d296a97fdbcd8357a518b906da9811a89096
    query = ' '.join(request.args.get('query', '').split())

    print('---')
    print('Item Search:', query)

    # XXX DUPLICATED IN TEMPLATES
    TYPEAHEAD_LIMIT = 15

    # TODO : Normalize accents...
    #
    # éèêë => e
    # " is stored as '' in the actual data
    # . and , should mean the same thing for numbers

    # Use up to 20 unique terms (avoid errors if the query is eg. a
    # copy-paste mistake)
    terms = list(extract_terms(query))[:20]

    if not terms:
         return jsonify({
             'results': [],
             'total_count': 0,
         })

    hard_terms = []
    optional_terms = []

    for t in terms:
        # Non-alphabetic search terms are mandatory
        if re.search('[^a-zA-Z]{2,}', t) or len(t) < 2:
            hard_terms.append(t)
        else:
            optional_terms.append(t)

    print('Terms', terms)
    print('Hard:', hard_terms)
    print('Opt:', optional_terms)

    q = InventoryItem.select()

    score_sql_expr = 0

    hard_constraints = None
    or_where = None

    if hard_terms:
        # Item codes are stored in a json field
        json_codes_table = InventoryItem.item_codes.tree().alias('tree')

        q = q.from_(InventoryItem, json_codes_table)
        q = q.group_by(InventoryItem.id)

        for t in hard_terms:
            cond = (InventoryItem.name.contains(t) | json_codes_table.c.value.startswith(t))

            score_sql_expr = score_sql_expr + cond
            hard_constraints = cond if hard_constraints is None else (hard_constraints & cond)

    for t in optional_terms:
        cond = InventoryItem.name.contains(t)
        or_where = cond if or_where is None else or_where | cond

        score_sql_expr = score_sql_expr + cond

    # Score bonus if the full search is in the name
    score_sql_expr += 3 * InventoryItem.name.contains(query)

    # Score bonus if the name is the whole search
    score_sql_expr += 4 * (InventoryItem.name == query)

    # TODO : Bonuses for whole words (\b...\b)

    # Note : improving scores for matching parts of the query might be
    # possible by using find_longest_match() between the full terms
    # and the item name
    # https://docs.python.org/3/library/difflib.html#difflib.SequenceMatcher.find_longest_match

    if hard_constraints is not None and or_where is not None:
        q = q.where(hard_constraints & or_where)
    elif hard_constraints is not None:
        q = q.where(hard_constraints)
    elif or_where is not None:
        q = q.where(or_where)

    q = q.where(InventoryItem.archived==False)

    q = q.order_by(score_sql_expr.desc())

    q = q.select(
        InventoryItem.id,
        InventoryItem.name,
        InventoryItem.category,
        InventoryItem.subcategory,
        InventoryItem.price,
        InventoryItem.always_edit,
        score_sql_expr.alias('score'))

    print(q)

    total_count = q.count()

    if 'full' not in request.args:
        q = q.limit(TYPEAHEAD_LIMIT)

    sorted_results = list(q.dicts())

    return jsonify({
        'results': sorted_results,
        'total_count': total_count,
    })

@app.route('/api/search/inventory_items/category/', methods=['GET'])
@available_if_cashregister_closed
def api_get_inventory_items_category():
    if 'category' not in request.args or \
       'subcategory' not in request.args:
        abort(403)

    category = request.args['category']
    subcategory = request.args['subcategory']

    query = InventoryItem.select().where(
            (InventoryItem.category == category) &
            (InventoryItem.subcategory == subcategory)&
            (InventoryItem.archived == 0))
    total_count = query.count()

    query = query.dicts()
    results = []
    for item in query:
        results.append(item)

    return jsonify({
        'results': results,
        'total_count': total_count,
    })

@app.route('/api/search/inventory_items/membership/', methods=['GET'])
@available_if_cashregister_closed
def api_get_inventory_items_special_meaning():
    query = InventoryItem.select().where((InventoryItem.special_meaning == 'membership') & (InventoryItem.archived == 0))
    total_count = query.count()

    query = query.dicts()

    results = []
    for item in query:
        results.append(item)

    return jsonify({
        'results': results,
        'total_count': total_count,
    })

@app.route('/api/search/workorder/', methods=['GET'])
@available_if_cashregister_closed
def api_get_workorder():

    if 'query' not in request.args:
        abort(403)

    type_color = 'd-none'
    type_name = ''
    client_name = "Direct Sale"

    workorder = Workorder.select().where(Workorder.id == int(request.args['query']))

    if workorder.count() == 0:
        return jsonify([])



    workorder = workorder.get()
    if workorder.type:
        type = app.config['EXTRA_WORKORDER_TYPES'][workorder.type]
        type_color = 'bg-primary'
        type_name = type['description_one']

    client = Client.select().where(Client.id == workorder.client)

    if client.count() > 0:
        client = client.get()
        client_name = client.first_name + ' ' + client.last_name



    return jsonify([{
        'id': workorder.id,
        'description': workorder.bike_description,
        'client_name' : client_name,
        'status_name' : workorder.status.name,
        'status_color' : workorder.status.color,
        'type_name': type_name,
        'type_color': type_color,
    }])

@app.route('/api/search/clients/', methods=['GET'])
@available_if_cashregister_closed
@timefunc
def api_get_clients():

    if 'query' not in request.args:
        abort(403)

    query = ' '.join(request.args['query'].split())
    terms = list(extract_terms(query))[:5]

    if not terms:
        return jsonify([])

    q = Client.select(Client.id, Client.first_name, Client.last_name, Client.phone)

    conditions = []

    for t in terms:
        conditions.append(Client.first_name.startswith(t))
        conditions.append(Client.last_name.startswith(t))

    conditions = or_where_conds(conditions)

    q = q.where(conditions)

    print(q)
    q = q.dicts()

    results = []

    for item in q:

        score = 0

        full_match = query in item['first_name'] + ' ' + item['last_name']

        score_fullname = does_match(query, item['first_name'] + ' ' + item['last_name'])
        score_firstname = does_match(query, item['first_name'])
        score_lastname = does_match(query, item['last_name'])

        score += (3 * score_fullname + score_firstname + score_lastname) * 3 + 10 if full_match else 0

        for t in terms:
            score_fullname = does_match(t, item['first_name'] + ' ' + item['last_name'])
            score_firstname = does_match(t, item['first_name'])
            score_lastname = does_match(t, item['last_name'])

            score += 3 * score_fullname + score_firstname + score_lastname

        item['score'] = score
        results.append((item, score))

    sorted_results = [
        item
        for item, score in sorted(results, key=lambda x: x[1], reverse=True)
    ]

    return jsonify(sorted_results)

@app.route('/api/refund/', methods=['POST'])
def api_refund():

    data = json.loads(request.form['items'])

    refunded_items_id = data['refunded_items_id']
    old_workorder_id = data['workorder_id']
    old_workorder = get_object_or_404(Workorder, (Workorder.id==old_workorder_id))

    # Checks if the items have already been refuned
    workorder_items = []
    for item_id in refunded_items_id:
        workorder_item = get_object_or_404(WorkorderItem, (WorkorderItem.id==item_id))
        if not workorder_item.serialized()['refunded_in_workorder_id'] and workorder_item.refund_item_id is None:
            workorder_items.append(workorder_item)

    if len(workorder_items) == 0 or not old_workorder.paid:
        return '/' # current_workorder

    # Creates new workorder based on the original workorder
    new_workorder = Workorder.create(
        client_id=old_workorder.client_id,
        bike_description=old_workorder.bike_description,
        bike_serial_number=old_workorder.bike_serial_number,
        invoice_notes=old_workorder.invoice_notes,
        internal_notes=old_workorder.internal_notes,
    )

    for item in workorder_items:
        WorkorderItem.create(
                    name=item.name,
                    nb = -item.nb,
                    price= item.price,
                    cost = item.cost,
                    orig_price= item.price,
                    taxable=item.taxable,
                    refund_item_id=item.id,
                    workorder_id=new_workorder.id, inventory_item_id=item.inventory_item_id
                )


    return '/workorder/' + str(new_workorder.id)

@app.route('/iframe')
def iframe():

    url = request.args.get('path', '/')

    if not validate_redirect_url(url):
        abort(403, "Invalid path")

    return render_template('iframe.html', url=url)

@app.route('/api/similar-client/', methods=["GET"])
def similar_client():
    if ('email' not in request.args or
        'phone' not in request.args or
        'first_name' not in request.args or
        'last_name'not in request.args):
        abort(403)

    import re
    phone = re.sub('[^0-9]','',request.args['phone'])
    email = request.args['email'].strip()
    first_name = request.args['first_name'].strip()
    last_name = request.args['last_name'].strip()

    query = Client.select(
        Client.id,
        Client.first_name,
        Client.last_name,
        Client.phone,
        ).where(
            ((Client.email == email) & (Client.email != "")) |
             ((Client.phone == phone)& (Client.phone != "")) |
             (((fn.Lower(Client.first_name) == first_name) & (Client.first_name != "")) &
              ((fn.Lower(Client.last_name) == last_name) & (Client.last_name != "")))
        ).limit(5)

    query = query.dicts()
    results = []
    for item in query:
        results.append(item)

    return jsonify({
        'results': results,
    })
